﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete;

namespace TrelloTestAutomation.Objects.Pages.DeleteBoardConfirmationPage
{
    public class DeleteBoardConfirmationPage
    {
        readonly IWebDriver _driver;
        readonly DeleteBoardConfirmationPageHTML _html;

        public DeleteBoardConfirmationPage(IWebDriver driver)
        {
            _driver = driver;
            _html = new DeleteBoardConfirmationPageHTML(driver);
        }

        private DeleteBoardPopup OpenFinalDeletingMenu()
        {
            _html.DeleteLink().Click();
            return new DeleteBoardPopup(_driver);

        }
        public DeletedWorkPagePage.DeletedWorkPagePage ConfirmClosing()
        {
            DeleteBoardPopup popup = OpenFinalDeletingMenu();
            return popup.PermanentlyDelete();
        }

        public void Reopen()
        {
            _html.ReopenLink().Click();
        }
    }
}
