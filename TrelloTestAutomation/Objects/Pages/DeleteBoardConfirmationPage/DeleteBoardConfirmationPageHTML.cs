﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.DeleteBoardConfirmationPage
{
    public sealed class DeleteBoardConfirmationPageHTML : BasePage
    {
        readonly Xpath _deleteLocator = new Xpath("a").Descendant().WithClasses("quiet js-delete").Combined();
        readonly Xpath _reopenLocator = new Xpath("a").Descendant().WithClass("js-reopen").Combined();
        IWebDriver _driver;

        public DeleteBoardConfirmationPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement DeleteLink()
        {
            return GetElement(By.XPath(_deleteLocator.CompleteXpath));
        }

        public PageElement ReopenLink()
        {
            return GetElement(By.XPath(_reopenLocator.CompleteXpath));
        }
    }
}
