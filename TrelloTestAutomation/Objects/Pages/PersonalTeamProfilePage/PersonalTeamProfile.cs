﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Boards;
using TrelloTestAutomation.Objects.Pages.CommonHTML;

namespace TrelloTestAutomation.Objects.Pages.PersonalTeamProfilePage
{
    public class PersonalTeamProfile : LoggedInPage.LoggedInPage, IProfilable
    {
        readonly IWebDriver _driver;
        private TeamSettings _teamSettings;
        private TeamMembers _teamMembers;
        private TeamBoards _teamBoards;
        readonly TeamProfileHTML _html;
        readonly string _teamName;

        public PersonalTeamProfile(IWebDriver driver, string teamName) : base(driver)
        {
            _driver = driver;
            _teamName = teamName;
            _teamSettings = new TeamSettings(driver, teamName);
            _teamMembers = new TeamMembers(driver);
            _teamBoards = new TeamBoards(driver);
            _html = new TeamProfileHTML(driver, _teamName);
        }

        public virtual TeamMembers GoToMemebers()
        {
            _html.MembersTabButton().Click();
            return _teamMembers;
        }

        public virtual TeamSettings GoToSettings()
        {
            _html.SettingsTabButton().Click();
            return _teamSettings;
        }

        public virtual TeamBoards GoToBoards()
        {
            _html.BoardsTabButton().Click();
            return _teamBoards;
        }

        public virtual void AddMember(string who)
        {
            _html.Username = who;
            GoToMemebers().AddMember(who);
        }

        public virtual void ChangeVisibility(Visibility toWhatVisibility)
        {
            GoToSettings().ChangeVisibility(toWhatVisibility);

        }

        public virtual void DeleteTeam()
        {
            GoToSettings().Delete();
        }

        public virtual void RemoveMember()
        {
            _html.RemoveMemberButton().Click();
            _html.ConfirmMemberRemovalButton().Click();
        }

        public virtual bool IsNamed()
        {
            return _html.IsPresent(_html.ProfileHeader);
        }

        public virtual bool IsPublicVisibilityDisplayed()
        {
            return _teamSettings.GetCurrentVisibility() == Visibility.publicVisibility ? true : false;
        }

        public virtual bool IsPrivateVisibilityDisplayed()
        {
            return _teamSettings.GetCurrentVisibility() == Visibility.privateVisibility ? true : false;
        }

        public virtual bool IsMemberAdded()
        {
            return _html.IsPresent(_html.NewMember);
        }
    }
}
