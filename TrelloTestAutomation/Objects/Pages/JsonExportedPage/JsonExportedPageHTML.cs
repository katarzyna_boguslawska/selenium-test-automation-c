﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.JsonExportedPage
{
    public sealed class JsonExportedPageHTML : BasePage
    {
        readonly Xpath _bodyLocator = new Xpath("body").Descendant().Combined();
        IWebDriver _driver;

        public JsonExportedPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement Body()
        {
            return GetElement(By.XPath(_bodyLocator.CompleteXpath.Substring(0, _bodyLocator.CompleteXpath.Length - 2)));
        }
    }
}
