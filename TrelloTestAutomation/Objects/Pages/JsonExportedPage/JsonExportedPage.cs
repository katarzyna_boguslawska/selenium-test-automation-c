﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu;

namespace TrelloTestAutomation.Objects.Pages.JsonExportedPage
{
    public class JsonExportedPage : BasePage
    {
        readonly IWebDriver _driver;
        readonly Card _card;
        readonly JsonExportedPageHTML _html;

        public JsonExportedPage(IWebDriver driver, Card card) : base(driver)
        {
            _driver = driver;
            _card = card;
            _html = new JsonExportedPageHTML(driver);
        }

        public bool IsJson()
        {
            return _html.Body().Text.StartsWith("{\"id\":\"");
        }

        public GroupWorkPage.GroupWorkPage UnexportFromJson(GroupWorkPage.GroupWorkPage workpage)
        {
            _driver.Navigate().Back();
            CardMenu menu = new CardMenu(_driver, _card);
            menu.HideMenu();
            return workpage;
        }
    }
}
