﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
namespace TrelloTestAutomation.Objects.Pages.LoginPage
{
    public class LoginPage
    {
        private IWebDriver _driver;
        private string _url;
        readonly LoginPageHTML _html;

        public LoginPage(IWebDriver driver)
        {
            _driver = driver;
            _url = "https://trello.com/login";
            _driver.Navigate().GoToUrl(_url);
            _html = new LoginPageHTML(_driver);

        }

        public bool IsTitleMatches()
        {
            string title = _html.Title().Text;
            return title.Equals("Log in to Trello") ? true : false;
        }

        public void CompleteLogin(String login)
        {
            try
            {
                PageElement loginInput = _html.UsernameField();
                loginInput.Type(login).ClickEnter();
            }
            catch (TimeoutException)
            {
                PageElement loginInput = _html.TUsernameField();
                if (_html.TUsernameField().IsClickable())
                {
                    loginInput.FocusAndType(login);
                    PageElement validateLoginButton = _html.TConfirm();
                    if (_html.TUsernameField().IsClickable())
                    {
                        validateLoginButton.Click();
                    }
                }
            }
        }

        public void CompletePassword(String password)
        {
            _html.PasswordField().FocusAndType(password);
        }

        public LoggedInPage.LoggedInPage Submit()
        {
            try
            {
                _html.LoginButton().Submit();
            }
            catch (TimeoutException)
            {
                PageElement submitButton = _html.TLoginButton();
                submitButton.Validate();
                submitButton.Submit();
            }
            return new LoggedInPage.LoggedInPage(_driver);
        }

        public void CompleteWrongLogin(String wrongLogin, string password)
        {
            try
            {
                PageElement loginInput = _html.UsernameField();
                loginInput.Type(wrongLogin).ClickEnter();
                CompletePassword(password);
            }
            catch (TimeoutException)
            {
                PageElement loginInput = _html.TUsernameField();
                if (_html.TUsernameField().IsClickable())
                {
                    loginInput.FocusAndType(wrongLogin);
                    PageElement validateLoginButton = _html.TConfirm();
                    if (_html.TUsernameField().IsClickable())
                    {
                        validateLoginButton.Click();
                    }
                }
            }
        }

        public FailedLoginPage.FailedLoginPage FailLogin()
        {
            try
            {
                _html.LoginButton().Submit();
            }
            catch (TimeoutException)
            {
                PageElement submitButton = _html.TLoginButton();
                submitButton.Validate();
                submitButton.Submit();
            }
            return new FailedLoginPage.FailedLoginPage(_driver);
        }

        public LoggedInPage.LoggedInPage LogIn(String login, string password)
        {
            CompleteLogin(login);
            CompletePassword(password);
            Submit();
            return new LoggedInPage.LoggedInPage(_driver);
        }

        public PasswordResetPage.PasswordResetPage RequestPasswordReset(String login)
        {
            try
            {
                _html.TUsernameField().FocusAndType(login);
                _html.TConfirm().Click();
            }
            catch (OpenQA.Selenium.WebDriverTimeoutException)
            {
                _html.PasswordResetLink().IsVisible();
            }
            _html.PasswordResetLink().Click();
            return new PasswordResetPage.PasswordResetPage(_driver);
        }
    }
}
