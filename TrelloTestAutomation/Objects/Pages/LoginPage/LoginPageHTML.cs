﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.LoginPage
{
    public sealed class LoginPageHTML : BasePage
    {
        readonly Xpath _titleLocator = new Xpath("h1").Descendant().Combined();
        readonly Xpath _usernameFieldLocator = new Xpath("input").Descendant().WithId("user").Combined();
        readonly Xpath _passwordFieldLocator = new Xpath("input").Descendant().WithId("password").Combined();
        readonly Xpath _loginButtonLocator = new Xpath("input").Descendant().WithId("login").Combined();
        readonly Xpath _passwordResetLinkLocator = new Xpath("a").Descendant().WithAttribute("href", "/forgot").Combined();

        //locators applicable to the "new" login page (the one with Taco));

        readonly Xpath _tUsernameFieldLocator = new Xpath("input").Descendant().WithId("login-identifier").Combined();
        readonly Xpath _tConfirmLocator = new Xpath("button").Descendant().WithClasses("button button-green js-login-button").Combined();
        readonly Xpath _tLoginButtonLocator = new Xpath("button").Descendant().WithClasses("button button-green js-password-button").Combined();

        IWebDriver _driver;

        public LoginPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement Title()
        {
            return GetElement(By.XPath(_titleLocator.CompleteXpath));
        }

        public PageElement UsernameField()
        {
            return GetElement(By.XPath(_usernameFieldLocator.CompleteXpath));
        }

        public PageElement PasswordField()
        {
            return GetElement(By.XPath(_passwordFieldLocator.CompleteXpath));
        }

        public PageElement LoginButton()
        {
            return GetElement(By.XPath(_loginButtonLocator.CompleteXpath));
        }

        public PageElement PasswordResetLink()
        {
            return GetElement(By.XPath(_passwordResetLinkLocator.CompleteXpath));
        }

        public PageElement TUsernameField()
        {
            return GetElement(By.XPath(_tUsernameFieldLocator.CompleteXpath));
        }

        public PageElement TConfirm()
        {
            return GetElement(By.XPath(_tConfirmLocator.CompleteXpath));
        }

        public PageElement TLoginButton()
        {
            return GetElement(By.XPath(_tLoginButtonLocator.CompleteXpath));
        }
    }
}
