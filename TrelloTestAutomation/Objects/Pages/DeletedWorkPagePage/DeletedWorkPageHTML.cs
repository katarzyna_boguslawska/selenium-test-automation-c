﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.DeletedWorkPagePage
{
    public sealed class DeletedWorkPageHTML : BasePage
    {
        readonly Xpath _notFoundLocator = new Xpath("h1").Descendant().WithExactText("Board not found.").Combined();
        IWebDriver _driver;

        public DeletedWorkPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement NotFound()
        {
            return GetElement(By.XPath(_notFoundLocator.CompleteXpath));
        }
    }
}
