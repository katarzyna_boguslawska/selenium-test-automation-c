﻿using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.DeletedWorkPagePage
{
    public class DeletedWorkPagePage : LoggedInPage.LoggedInPage
    {
        readonly IWebDriver _driver;
        readonly DeletedWorkPageHTML _html;


        public DeletedWorkPagePage(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new DeletedWorkPageHTML(driver);
        }

        public bool IsDeleted()
        {
            return _html.IsPresent(_html.NotFound);
        }
    }
}
