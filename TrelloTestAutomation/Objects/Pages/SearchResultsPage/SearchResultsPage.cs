﻿using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.SearchResultsPage
{
    public class SearchResultsPage : LoggedInPage.LoggedInPage
    {
        readonly IWebDriver _driver;
        public string SearchPhrase { get; set; }
        private SearchResultsPageHTML _html;

        public SearchResultsPage(IWebDriver driver, string searchPhrase) : base(driver)
        {
            _driver = driver;
            SearchPhrase = searchPhrase;
            _html = new SearchResultsPageHTML(_driver, SearchPhrase);
        }

        public bool AreResultsPresent()
        {
            var allResults = _html.ResultLink();
            return allResults.Count > 0;
        }
    }
}
