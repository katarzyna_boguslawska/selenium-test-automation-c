﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.SearchResultsPage
{
    class SearchResultsPageHTML : BasePage
    {
        readonly Xpath _resultLink;
        readonly IWebDriver _driver;
        readonly string _searchPhrase;

        public SearchResultsPageHTML(IWebDriver driver, string searchPhrase) : base(driver)
        {
            _driver = driver;
            _searchPhrase = searchPhrase;
            var descendantNode = new Xpath("span").Descendant().WithAttribute("title", searchPhrase).Combined();
            _resultLink = new Xpath("a").Descendant().WithClasses("js-open-board compact-board-tile-link dark").HavingDescendant(descendantNode).Combined();
        }

        public List<PageElement> ResultLink()
        {
            return GetElements(By.XPath(_resultLink.CompleteXpath));
        }
    }
}
