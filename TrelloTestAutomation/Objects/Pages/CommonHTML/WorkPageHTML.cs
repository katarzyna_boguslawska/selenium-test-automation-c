﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace TrelloTestAutomation.Objects.Pages.CommonHTML
{
    public sealed class WorkPageHTML : BasePage
    {
        readonly Xpath _listTitleInputLocator = new Xpath("input").Descendant().WithClass("list-name-input").Combined();
        readonly Xpath _saveListLocator = new Xpath("input").Descendant().WithClasses("primary mod-list-add-button js-save-edit").Combined();
        readonly Xpath _quitAdding = new Xpath("a").Descendant().WithClasses("icon-large icon-close dark-hover js-cancel-edit").WithAttribute("href", "#").Combined();
        readonly Xpath _showMenuLocator = new Xpath("span").Descendant().WithClasses("board-header-btn-text u-text-underline").WithText("Show Menu").Combined();
        readonly Xpath _hideMenuLocator = new Xpath("a").Descendant().WithClasses("board-menu-header-close-button icon-lg icon-close js-hide-sidebar").Combined();
        readonly Xpath _genericListLocator = new Xpath("div").Descendant().WithClass("js-list list-wrapper").Combined();
        readonly Xpath _closeMenuLocator = new Xpath("a").Descendant().WithAttribute("title", "Close the board menu.").Combined();
        readonly Xpath _titleLinkLocator = new Xpath("a").Descendant().WithClasses("board-header-btn board-header-btn-name js-rename-board").Combined();
        readonly Xpath _newNameInputLocator = new Xpath("input").Descendant().WithClasses("js-board-name js-autofocus").Combined();
        readonly Xpath _nameChangeButtonLocator = new Xpath("input").Descendant().WithClasses("primary wide js-rename-board").WithAttribute("value", "Rename").Combined();
        readonly Xpath _bodyLocator = new Xpath("body").Descendant().Combined();
        Xpath _workPageTitleLocator;
        public string BoardName { get; set; }

        public WorkPageHTML(IWebDriver driver, string workPageTitle) : base(driver)
        {
            BoardName = workPageTitle;
            _workPageTitleLocator = new Xpath("span").Descendant().WithClass("board-header-btn-text").WithText(BoardName).Combined();
        }

        public PageElement ListContainer(string listTitle)
        {
            var descendantXpath = new Xpath("textarea").Descendant().WithExactText(listTitle).Combined();
            var listContainerXpath = new Xpath("div").Descendant().WithClasses("list js-list-content").HavingDescendant(descendantXpath).Combined();
            return GetElement(By.XPath(listContainerXpath.CompleteXpath));
        }

        public PageElement ListTitleInput()
        {
            return GetElement(By.XPath(_listTitleInputLocator.CompleteXpath));
        }

        public PageElement SaveListButton()
        {
            return GetElement(By.XPath(_saveListLocator.CompleteXpath));
        }

        public PageElement QuitAddingButton()
        {
            return GetElement(By.XPath(_quitAdding.CompleteXpath));
        }

        public PageElement ShowMenuLink()
        {
            return GetElement(By.XPath(_showMenuLocator.CompleteXpath));
        }

        public PageElement HideMenuLink()
        {
            return GetElement(By.XPath(_hideMenuLocator.CompleteXpath));
        }

        public List<PageElement> GenericList()
        {
            return GetElements(By.XPath(_genericListLocator.CompleteXpath));
        }

        public PageElement CloseMenuButton()
        {
            return GetElement(By.XPath(_closeMenuLocator.CompleteXpath));
        }

        public PageElement TitleLink()
        {
            return GetElement(By.XPath(_titleLinkLocator.CompleteXpath));
        }

        public PageElement NewNameInput()
        {
            return GetElement(By.XPath(_newNameInputLocator.CompleteXpath));
        }

        public PageElement NameChangeButton()
        {
            return GetElement(By.XPath(_nameChangeButtonLocator.CompleteXpath));
        }

        public PageElement Body()
        {
            return GetElement(By.XPath(_bodyLocator.CompleteXpath.Substring(0, _bodyLocator.CompleteXpath.Length - 2)));
        }

        public PageElement WorkPageTitle()
        {
            _workPageTitleLocator = new Xpath("span").Descendant().WithClass("board-header-btn-text").WithText(BoardName).Combined();
            return GetElement(By.XPath(_workPageTitleLocator.CompleteXpath));
        }
    }
}
