﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonHTML
{
    public sealed class TeamProfileHTML : BasePage
    {
        readonly Xpath _membersTabLocator = new Xpath("a").Descendant().WithClasses("tabbed-pane-nav-item-button js-org-members").WithExactText("Members").Combined();
        readonly Xpath _settingsTabLocator = new Xpath("a").Descendant().WithExactText("Settings").Combined();
        readonly Xpath _boardsTabLocator = new Xpath("a").Descendant().WithExactText("Boards").Combined();
        readonly Xpath _removeMemberLocator = new Xpath("a").Descendant().WithClasses("option button-link remove-button").Combined();
        readonly Xpath _confirmMemberRemovalLocator = new Xpath("a").Descendant().WithClass("js-soft-remove").Combined();
        readonly Xpath _profileHeaderLocator;
        Xpath _newMemberLocator;
        readonly string _teamName;
        public string Username { get; set; }
        readonly IWebDriver _driver;

        public TeamProfileHTML(IWebDriver driver, string teamName) : base(driver)
        {
            _driver = driver;
            _teamName = teamName;
            var descendantNode = new Xpath("h1").Descendant().WithExactText(_teamName).Combined();
            _profileHeaderLocator = new Xpath("div").Descendant().WithClass("tabbed-pane-header-details-name").HavingDescendant(descendantNode).Combined();
        }

        public PageElement MembersTabButton()
        {
            return GetElement(By.XPath(_membersTabLocator.CompleteXpath));
        }

        public PageElement SettingsTabButton()
        {
            return GetElement(By.XPath(_settingsTabLocator.CompleteXpath));
        }

        public PageElement BoardsTabButton()
        {
            return GetElement(By.XPath(_boardsTabLocator.CompleteXpath));
        }

        public PageElement RemoveMemberButton()
        {
            return GetElement(By.XPath(_removeMemberLocator.CompleteXpath));
        }

        public PageElement ConfirmMemberRemovalButton()
        {
            return GetElement(By.XPath(_confirmMemberRemovalLocator.CompleteXpath));
        }

        public PageElement ProfileHeader()
        {
            return GetElement(By.XPath(_profileHeaderLocator.CompleteXpath));
        }

        public PageElement NewMember()
        {
            var who = PrepareUsername();
            _newMemberLocator = new Xpath("span").Descendant().WithTextStarting(who).Combined();
            return GetElement(By.XPath(_newMemberLocator.CompleteXpath));
        }

        private string PrepareUsername()
        {
            var who = "@" + Username.Substring(0, Username.IndexOf("@"));
            if (who.Contains("_"))
            {
                return who.Replace("_", "");
            }
            return who;
        }
    }
}
