﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.PasswordResetPage
{
    public sealed class PasswordResetPageHTML : BasePage
    {
        readonly Xpath _emailInputLocator = new Xpath("input").Descendant().WithId("email").Combined();
        readonly Xpath _submitButtonLocator = new Xpath("input").Descendant().WithId("submit").Combined();
        readonly Xpath _resetHeadingLocator = new Xpath("h1").Descendant().Combined();
        readonly Xpath _resetDetailsLocator = new Xpath("p").Descendant().WithId("feedback").Combined();

        readonly Xpath _tEmailInputLocator = new Xpath("input").Descendant().WithId("forgot-email").Combined();
        readonly Xpath _tSubmitButtonLocator = new Xpath("button").Descendant().WithClasses("button button-green js-forgot-button").Combined();
        readonly Xpath _tEmailNotificationLocator = new Xpath("p").Descendant().WithClasses("js-check-email-text text-center").Combined();

        IWebDriver _driver;

        public PasswordResetPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement EmailInput()
        {
            return GetElement(By.XPath(_emailInputLocator.CompleteXpath));
        }

        public PageElement SubmitButton()
        {
            return GetElement(By.XPath(_submitButtonLocator.CompleteXpath));
        }

        public PageElement ResetHeading()
        {
            return GetElement(By.XPath(_resetHeadingLocator.CompleteXpath.Substring(0, _resetHeadingLocator.CompleteXpath.Length - 2)));
        }

        public PageElement ResetDetails()
        {
            return GetElement(By.XPath(_resetDetailsLocator.CompleteXpath));
        }

        public PageElement TEmailInput()
        {
            return GetElement(By.XPath(_tEmailInputLocator.CompleteXpath));
        }

        public PageElement TSubmitButton()
        {
            return GetElement(By.XPath(_tSubmitButtonLocator.CompleteXpath));
        }

        public PageElement TEmailNotification()
        {
            return GetElement(By.XPath(_tEmailNotificationLocator.CompleteXpath));
        }
    }
}
