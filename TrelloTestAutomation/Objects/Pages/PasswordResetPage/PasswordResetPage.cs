﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.PasswordResetPage
{
    public class PasswordResetPage
    {
        readonly string _url;
        private PasswordResetPageHTML _html;

        public PasswordResetPage(IWebDriver driver)
        {
            _url = "https://trello.com/forgot";
            _html = new PasswordResetPageHTML(driver);
        }

        public bool IsEmailSent()
        {
            bool isHeadingAvailable = _html.IsPresent(_html.ResetHeading);
            bool isDetailsAvailable = _html.IsPresent(_html.ResetDetails);
            bool isEmailSent = _html.IsPresent(_html.TEmailNotification);
            bool basicCondition = isHeadingAvailable & isDetailsAvailable;
            return basicCondition || isEmailSent ? true : false;
        }

        public void CompleteDataForReset(string login)
        {
            try
            {
                PageElement emailInput = _html.EmailInput();
                emailInput.Type(login);
                _html.SubmitButton().Click();
            }
            catch (WebDriverTimeoutException)
            {
                _html.TSubmitButton().Click();
            }
        }
    }
}
