﻿using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Boards
{
    public class TeamBoards
    {
        readonly IWebDriver _driver;
        readonly TeamBoardsHTML _html;

        public TeamBoards(IWebDriver driver)
        {
            _driver = driver;
            _html = new TeamBoardsHTML(driver);
        }
    }
}
