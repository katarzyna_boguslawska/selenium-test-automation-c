﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Boards
{
    public sealed class TeamBoardsHTML : BasePage
    {
        readonly Xpath _createBoardLocator = new Xpath("a").Descendant().WithClass("board-tile mod-add").WithAttribute("href", "#").Combined();
        readonly IWebDriver _driver;

        public TeamBoardsHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CreateBoardButton()
        {
            return GetElement(By.XPath(_createBoardLocator.CompleteXpath));
        }
    }
}
