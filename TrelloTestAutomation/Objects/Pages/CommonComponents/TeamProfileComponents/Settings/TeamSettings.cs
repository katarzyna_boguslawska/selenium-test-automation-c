﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility;
using System;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings
{
    public class TeamSettings : BasePage, IDeletable
    {
        readonly string _teamName;
        readonly IWebDriver _driver;
        readonly TeamSettingsHTML _html;

        public TeamSettings(IWebDriver driver, string _teamName) : base(driver)
        {
            _driver = driver;
            this._teamName = _teamName;
            this._html = new TeamSettingsHTML(driver);
        }

        public Visibility GetCurrentVisibility()
        {
            bool isPrivate = _html.IsPresent(By.XPath(Visibility.privateVisibility.CurrentVisibilityLocator.CompleteXpath));
            return isPrivate ? Visibility.privateVisibility : Visibility.publicVisibility;
        }

        private DeletionPopup OpenDeletingPopup()
        {
            ScrollTo(_html.DeletingTeamLink());
            _html.DeletingTeamLink().Click();
            return new DeletionPopup(_driver);
        }

        public virtual void Delete()
        {
            DeletionPopup popup = OpenDeletingPopup();
            popup.Delete();
        }

        private VisibilityPopup OpenVisibilityPopup()
        {
            _html.ChangeVisibilityButton().Click();
            return new VisibilityPopup(_driver);
        }

        public void ChangeVisibility(Visibility requestedVisibility)
        {
            VisibilityPopup popup = OpenVisibilityPopup();
            popup.ChangeVisibility(requestedVisibility);
        }
    }
}
