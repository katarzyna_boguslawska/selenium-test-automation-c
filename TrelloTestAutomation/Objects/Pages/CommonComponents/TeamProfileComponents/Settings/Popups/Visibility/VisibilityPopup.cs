﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility
{
    public class VisibilityPopup : BasePage, ICloseable
    {
        readonly IWebDriver _driver;
        readonly VisibilityPopupHTML _html;

        public VisibilityPopup(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new VisibilityPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.GetCloseButton().Click();
        }

        public void ChangeVisibility(FixedChoices.Visibility requestedVisibility)
        {
            _html.VisibilityButton(requestedVisibility).Click();
        }
    }
}
