﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Visibility
{
    public sealed class VisibilityPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly IWebDriver _driver;

        public VisibilityPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement GetCloseButton() { return GetElement(By.XPath(_closeLocator.CompleteXpath)); }

        public PageElement VisibilityButton(FixedChoices.Visibility visibility)
        {
            return GetElement(By.XPath(visibility.VisibilityButtonLocator.CompleteXpath));
        }

        public PageElement VisibilityLabel(FixedChoices.Visibility visibility)
        {
            return GetElement(By.XPath(visibility.CurrentVisibilityLocator.CompleteXpath));
        }
    }
}
