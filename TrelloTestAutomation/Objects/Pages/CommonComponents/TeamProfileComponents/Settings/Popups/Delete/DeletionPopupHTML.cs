﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete
{
    public sealed class DeletionPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _confirmDeletingLocator = new Xpath("input").Descendant().WithClasses("js-confirm full negate").WithAttribute("type", "submit").Combined();
        readonly IWebDriver _driver;

        public DeletionPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement ConfirmDeletingButton()
        {
            return GetElement(By.XPath(_confirmDeletingLocator.CompleteXpath));
        }
    }
}
