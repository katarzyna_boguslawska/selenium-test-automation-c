﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings.Popups.Delete
{
    public class DeletionPopup : ICloseable, IDeletable
    {
        private IWebDriver _driver;
        private DeletionPopupHTML _html;

        public DeletionPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new DeletionPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public virtual void Delete()
        {
            _html.ConfirmDeletingButton().Click();
        }
    }
}
