﻿using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.FixedChoices;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings
{
    public sealed class TeamSettingsHTML : BasePage
    {
        static readonly Xpath _descendantNodeVisibility = new Xpath("span").Descendant().WithExactText("Change").Combined();
        static readonly Xpath _descendantNodeDelete = new Xpath("span").Descendant().WithExactText("Delete this team?").Combined();
        readonly Xpath _changeVisibilityLocator = new Xpath("a").Descendant().WithClasses("button-link u-text-align-center").HavingDescendant(_descendantNodeVisibility).Combined();
        readonly Xpath _deletingTeamLocator = new Xpath("a").Descendant().WithClass("quiet-button").HavingDescendant(_descendantNodeDelete).Combined();
        readonly IWebDriver _driver;

        public TeamSettingsHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement ChangeVisibilityButton()
        {
            return GetElement(By.XPath(_changeVisibilityLocator.CompleteXpath));
        }

        public PageElement DeletingTeamLink()
        {
            return GetElement(By.XPath(_deletingTeamLocator.CompleteXpath));
        }

        public PageElement VisibilityButton(Visibility visibility)
        {
            return GetElement(By.XPath(visibility.VisibilityButtonLocator.CompleteXpath));
        }

        public PageElement VisibilityLabel(Visibility visibility)
        {
            return GetElement(By.XPath(visibility.CurrentVisibilityLocator.CompleteXpath));
        }
    }
}
