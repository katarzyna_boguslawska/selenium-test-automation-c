﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups
{
    public class MemberPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly MemberPopupHTML _html;

        public MemberPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new MemberPopupHTML(_driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public void AddMember(string member)
        {
            PageElement memberNameInput = _html.NameInput();
            memberNameInput.Type(member);
            try
            {
                _html.SendInvitationButton().Click();
            }
            catch (WebDriverTimeoutException)
            {
                memberNameInput.ClickEnter();
            }
        }
    }
}
