﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups
{
    public sealed class MemberPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _nameInputLocator = new Xpath("input").Descendant().WithClasses("js-search-input js-autofocus").WithAttribute("type", "text").Combined();
        readonly Xpath _sendInvitationLocator = new Xpath("input").Descendant().WithClasses("wide primary js-send-email-invite").WithAttribute("type", "submit").Combined();
        readonly IWebDriver _driver;

        public MemberPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement NameInput()
        {
            return GetElement(By.XPath(_nameInputLocator.CompleteXpath));
        }

        public PageElement SendInvitationButton() { return GetElement(By.XPath(_sendInvitationLocator.CompleteXpath)); }
    }
}
