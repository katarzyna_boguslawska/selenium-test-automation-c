﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members
{
    public sealed class TeamMembersHTML : BasePage
    {
        static readonly Xpath _descendantNode = new Xpath("span").Descendant().WithExactText("Add by Name or Email").Combined();
        readonly Xpath _addByNameLocator = new Xpath("a").Descendant().WithClasses("primary button-link autowrap").HavingDescendant(_descendantNode).Combined();
        readonly Xpath _genericMember = new Xpath("*").Descendant().WithClass("member-list-item-detail");
        readonly IWebDriver _driver;

        public TeamMembersHTML(IWebDriver driver): base(driver)
        {
            _driver = driver;
        }

        public PageElement AddByNameButton()
        {
            return GetElement(By.XPath(_addByNameLocator.CompleteXpath));
        }

        public List<PageElement> GenericMember()
        {
            return GetElements(By.XPath(_genericMember.CompleteXpath));
        }
    }
}
