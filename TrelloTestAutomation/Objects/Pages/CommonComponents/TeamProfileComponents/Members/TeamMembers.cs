﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members.Popups;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members
{
    public class TeamMembers : BasePage
    {
        readonly IWebDriver _driver;
        readonly TeamMembersHTML _html;

        public TeamMembers(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            this._html = new TeamMembersHTML(driver);
        }

        private MemberPopup OpenMemberPopup()
        {
            _html.AddByNameButton().ClickEnter();
            return new MemberPopup(_driver);
        }

        public void AddMember(string member)
        {
            MemberPopup popup = OpenMemberPopup();
            popup.AddMember(member);
        }

        public int CountMembers()
        {
            return _html.GenericMember().Count;
        }

        public bool CheckIfMembersNumberIncreased(int greaterThanWhat)
        {
            int attemptsNo = (int)(3 / 0.2);
            for (int i = 0; i <= attemptsNo; i++)
            {
                if (CountMembers() > greaterThanWhat)
                {
                    return true;
                }
                else
                {
                    SlowDown(200);
                }
            }
            return false;
        }
    }
}
