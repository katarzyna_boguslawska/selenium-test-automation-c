﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List
{
    public sealed class ListHTML : BasePage
    {
        readonly Xpath _archiveListLocator;
        readonly Xpath _listContainerLocator;
        readonly Xpath _listHeaderAntecedentLocator;
        readonly Xpath _moreMenuLocator;
        readonly Xpath _addCardLocator;
        readonly Xpath _cardTextLocator;
        readonly Xpath _addLocator;
        readonly Xpath _quitAddingLocator;
        readonly Xpath _listHeaderLocator;
        readonly string _listTitle;
        readonly IWebDriver _driver;

        public ListHTML(IWebDriver driver, string listTitle) : base(driver)
        {
            _driver = driver;
            _listTitle = listTitle;
            _listHeaderLocator = new Xpath("h2").Descendant().WithText(listTitle).Combined();
            _listHeaderAntecedentLocator = new Xpath("h2").Descendant().WithText(listTitle).Grandparent().Combined();
            _listContainerLocator = new Xpath("div").Descendant().WithClass("js-list list-wrapper").HavingDescendant(_listHeaderLocator).Combined();
            _archiveListLocator = new Xpath("a").Descendant().WithClass("js-close-list").HavingAntecedent(_listHeaderAntecedentLocator).Combined();
            _moreMenuLocator = new Xpath("a").Descendant().WithClass("list-header-extras-menu dark-hover js-open-list-menu").HavingAntecedent(_listHeaderAntecedentLocator).Combined();
            _addCardLocator = new Xpath("a").Descendant().WithClasses("open-card-composer js-open-card-composer").HavingAntecedent(_listContainerLocator).Combined();
            _cardTextLocator = new Xpath("textarea").Descendant().WithClasses("list-card-composer-textarea js-card-title").HavingAntecedent(_listHeaderAntecedentLocator).Combined();
            _addLocator = new Xpath("input").Descendant().WithClasses("primary confirm mod-compact js-add-card").HavingAntecedent(_listHeaderAntecedentLocator).Combined();
            _quitAddingLocator = new Xpath("a").Descendant().WithClasses("icon-lg icon-close dark-hover js-cancel").HavingAntecedent(_listHeaderAntecedentLocator).Combined();
        }

        public PageElement CardContainer(string textOnCard)
        {
            var descendant = new Xpath("*").Descendant().WithExactText(textOnCard).Combined();
            var cardXpath = new Xpath("a").Descendant().WithClass("list-card").HavingAntecedent(_listHeaderAntecedentLocator).HavingDescendant(descendant).Combined();
            return GetElement(By.XPath(cardXpath.CompleteXpath));
        }

        public PageElement ArchiveListButton()
        {
            return GetElement(By.XPath(_archiveListLocator.CompleteXpath));
        }

        public PageElement ListContainer()
        {
            return GetElement(By.XPath(_listContainerLocator.CompleteXpath));
        }

        public PageElement MoreMenuButton()
        {
            return GetElement(By.XPath(_moreMenuLocator.CompleteXpath));
        }

        public PageElement AddCardButton()
        {
            return GetElement(By.XPath(_addCardLocator.CompleteXpath));
        }

        public PageElement CardTextButton()
        {
            return GetElement(By.XPath(_cardTextLocator.CompleteXpath));
        }

        public PageElement AddButton()
        {
            return GetElement(By.XPath(_addLocator.CompleteXpath));
        }

        public PageElement QuitAddingButton()
        {
            return GetElement(By.XPath(_quitAddingLocator.CompleteXpath));
        }
    }
}
