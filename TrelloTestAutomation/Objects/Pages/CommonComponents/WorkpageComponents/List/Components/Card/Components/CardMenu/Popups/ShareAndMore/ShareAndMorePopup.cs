﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore
{
    public class ShareAndMorePopup : ICloseable
    {
        readonly IWebDriver _driver;
        private ShareAndMorePopupHTML _html;

        public ShareAndMorePopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new ShareAndMorePopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public JsonExportedPage.JsonExportedPage ExportToJson(Card card)
        {
            _html.JsonExportOption().Click();
            return new JsonExportedPage.JsonExportedPage(_driver, card);
        }

        private DeleteCardPopup OpenDeleteCardPopup()
        {
            _html.DeleteLink().Click();
            return new DeleteCardPopup(_driver);
        }

        public void DeleteCard()
        {
            OpenDeleteCardPopup().DeleteCard();
        }
    }
}
