﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist
{
    public sealed class ChecklistPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClass("pop-over is-shown").Combined();
        readonly Xpath _checklistTitleLocator = new Xpath("input").Descendant().WithAttribute("id", "id-checklist").Combined();
        readonly Xpath _addChecklistLocator = new Xpath("input").Descendant().WithClasses("primary wide confirm js-add-checklist").Combined();
        readonly IWebDriver _driver;

        public ChecklistPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement ChecklistTitleInput()
        {
            return GetElement(By.XPath(_checklistTitleLocator.CompleteXpath));
        }

        public PageElement AddChecklistButton()
        {
            return GetElement(By.XPath(_addChecklistLocator.CompleteXpath));
        }
    }
}
