﻿using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment
{
    public class Attachment
    {
        readonly IWebDriver _driver;
        private string _link { get; }
        private AttachmentHTML _attachmentHTML;

        public Attachment(IWebDriver driver, string link)
        {
            _driver = driver;
            _link = link;
            _attachmentHTML = new AttachmentHTML(_driver, _link);
        }

        public bool IsAttachmentPresent()
        {
            return _attachmentHTML.IsPresent(_attachmentHTML.AttachmentContainer);
        }
    }
}
