﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate
{
    public class DueDatePopup : ICloseable
    {
        private IWebDriver _driver;
        private DueDatePopupHTML _html;

        public DueDatePopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new DueDatePopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public void AddDueDate(String date)
        {
            _html.DateInput().FocusAndType(date).ClickEnter();
        }

        public void RemoveDueDate()
        {
            _html.RemoveDueDateButton().ClickEnter();
        }
    }
}
