﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment
{
    public sealed class DeleteCommentPopupHTML : BasePage
    {
        readonly Xpath _deleteLocator = new Xpath("input").Descendant().WithClasses("js-confirm full negate").WithAttribute("type", "submit").Combined();
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly IWebDriver _driver;

        public DeleteCommentPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement DeleteButton()
        {
            return GetElement(By.XPath(_deleteLocator.CompleteXpath));
        }
    }
}
