﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment
{
    public class PostedComment : IDeletable
    {
        readonly IWebDriver _driver;
        public string Text { get; set; }
        readonly PostedCommentHTML _html;

        public PostedComment(IWebDriver driver, string text)
        {
            _driver = driver;
            Text = text;
            _html = new PostedCommentHTML(driver, Text);
        }

        public bool CheckIfHasEmoji(Emoji emoji)
        {
            return _html.IsPresent(By.XPath(emoji.EmojiImage.CompleteXpath));
        }

        public string GetCommentText()
        {
            return _html.PostedComment().Text;
        }

        private DeleteCommentPopup OpenDeletePopup()
        {
            _html.DeleteLink().Click();
            return new DeleteCommentPopup(_driver);
        }
        public virtual void Delete()
        {
            DeleteCommentPopup popup = OpenDeletePopup();
            popup.Delete();
        }
    }
}
