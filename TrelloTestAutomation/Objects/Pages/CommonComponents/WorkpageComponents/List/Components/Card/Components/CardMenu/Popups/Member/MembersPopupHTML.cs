﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member
{
    public sealed class MembersPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _memberInputLocator = new Xpath("input").Descendant().WithClasses("js-search-mem js-autofocus").Combined();
        readonly Xpath _memberOptionLocator;
        readonly string _member;
        readonly IWebDriver _driver;

        public MembersPopupHTML(IWebDriver driver, string member) : base(driver)
        {
            _driver = driver;
            _member = member;
            _memberOptionLocator = new Xpath("a").Descendant().WithClasses("name js-select-member ").WithAttributeContaining("title", _member).Combined();
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement MemberInput()
        {
            return GetElement(By.XPath(_memberInputLocator.CompleteXpath));
        }

        public PageElement MemberOption()
        {
            return GetElement(By.XPath(_memberOptionLocator.CompleteXpath));
        }
    }
}
