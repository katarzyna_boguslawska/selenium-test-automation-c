﻿using OpenQA.Selenium;
using System.Collections.Generic;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist
{
    public sealed class ChecklistHTML : BasePage
    {
        readonly Xpath _checklistContainer;
        readonly Xpath _headerLocator;
        readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClass("window").Combined();
        readonly Xpath _deleteLinktLocator;
        readonly Xpath _closeChecklistDeletionPopupLocator = new Xpath("a").Descendant().WithClasses("icon-lg icon-close dialog-close-button js-close-window").Combined();
        readonly Xpath _deleteChecklistButtonLocator = new Xpath("input").Descendant().WithAttribute("value", "Delete Checklist").Combined();
        readonly Xpath _progressBarLocator;
        public List<Dictionary<string, ChecklistItemHTML>> ChecklistItemHTMLs;
        readonly Xpath _addedItemTextareaLocator;
        readonly Xpath _addButtonLocator;
        readonly Xpath _quitAddingButtonLocator;
        readonly Xpath _genericItem;
        readonly string _title;
        readonly IWebDriver _driver;

        public ChecklistHTML(IWebDriver driver, string checklistTitle) : base(driver)
        {
            _driver = driver;
            _title = checklistTitle;
            _headerLocator = new Xpath("h3").Descendant().WithExactText(_title).Combined();
            _checklistContainer = new Xpath("div").Descendant().WithExactClass("checklist").HavingAntecedent(_windowLocator).HavingDescendant(_headerLocator).Combined();
            _deleteLinktLocator = new Xpath("a").Descendant().WithClasses("hide-on-edit quiet js-confirm-delete").HavingAntecedent(_windowLocator).Combined();
            _progressBarLocator = new Xpath("span").Descendant().WithClasses("checklist-progress-percentage js-checklist-progress-percent").HavingAntecedent(_windowLocator).Combined();
            _addedItemTextareaLocator = new Xpath("textarea").Descendant().WithClasses("checklist-new-item-text js-new-checklist-item-input").HavingAntecedent(_windowLocator).Combined();
            _addButtonLocator = new Xpath("input").Descendant().WithClasses("primary confirm mod-submit-edit js-add-checklist-item").HavingAntecedent(_windowLocator).Combined();
            _quitAddingButtonLocator = new Xpath("a").Descendant().WithClasses("icon-lg icon-close dark-hover cancel js-cancel-checklist-item").HavingAntecedent(_windowLocator).Combined();
            _genericItem = new Xpath("div").Descendant().WithExactClass("checklist-item").HavingAntecedent(_checklistContainer).Combined();
            ChecklistItemHTMLs = new List<Dictionary<string, ChecklistItemHTML>>();
        }

        public PageElement DeleteChecklistButton()
        {
            return GetElement(By.XPath(_deleteLinktLocator.CompleteXpath));
        }

        public PageElement CloseDeletionPopupButton()
        {
            return GetElement(By.XPath(_closeChecklistDeletionPopupLocator.CompleteXpath));
        }

        public PageElement DeleteButton()
        {
            return GetElement(By.XPath(_deleteChecklistButtonLocator.CompleteXpath));
        }

        public PageElement ProgressBar()
        {
            return GetElement(By.XPath(_progressBarLocator.CompleteXpath));
        }

        public PageElement ItemContentField()
        {
            return GetElement(By.XPath(_addedItemTextareaLocator.CompleteXpath));
        }

        public PageElement AddItemButton()
        {
            return GetElement(By.XPath(_addButtonLocator.CompleteXpath));
        }

        public PageElement QuitAddingItemsButton()
        {
            return GetElement(By.XPath(_quitAddingButtonLocator.CompleteXpath));
        }

        public PageElement Header()
        {
            return GetElement(By.XPath(_headerLocator.CompleteXpath));
        }

        public List<PageElement> Items()
        {
            return GetElements(By.XPath(_genericItem.CompleteXpath));
        }

        public ChecklistItemHTML ItemHTML(string itemText)
        { 
            foreach(Dictionary<string, ChecklistItemHTML> html in ChecklistItemHTMLs)
            {
                if (html.ContainsKey(itemText)){
                    return html[itemText];
                }
            }
            throw new NotFoundException(string.Format("An item with the text {0} was not found.", itemText));
        }
    }
}
