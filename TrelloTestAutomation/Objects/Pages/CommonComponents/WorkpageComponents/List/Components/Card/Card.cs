﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card
{
    public class Card
    {
        readonly IWebDriver _driver;
        public string Text { get; set; }
        readonly CardHTML _html;
        public CardMenu Menu { get; set; }

        public Card(IWebDriver driver, string textOnCard)
        {
            _driver = driver;
            Text = textOnCard;
            _html = new CardHTML(driver, Text);
        }

        public CardMenu ShowCardMenu()
        {
            if (Menu == null)
            {
                _html.CardContainer().Click();
                Menu = new CardMenu(_driver, this);
            }
            else
            {
                _html.CardContainer().Click();
            }
            return Menu;
        }

        public CardMenu ShowCardMenu(bool noClick)
        {
            return Menu;
        }

        public void DragAndDropCard(Xpath destinationElement)
        {
            _html.CardContainer().DragTo(destinationElement);
        }

        public void DragAndDropCard(Card destination)
        {
            _html.CardContainer().DragTo(destination._html.CardContainer().Root);
        }

        public bool DueDateWasAdded()
        {
            try
            {
                PageElement dateIndicator = _html.FutureDueDateBadge();
                return true;
            } catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool AttachmentWasAdded()
        {
            try
            {
                PageElement attachmentIndicator = _html.ImageBadge();
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool HasLabelOfColor(Color color)
        {
            return _html.IsLabelPresent(color);
        }

        public bool MemberWasAdded(string who)
        {
            try
            {
                PageElement memberIndicator = _html.MemberOption(who);
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool HasNoComment()
        {
            ShowCardMenu().HideMenu();
            try
            {
                _html.CommentBadge();
                return false;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }
    }
}
