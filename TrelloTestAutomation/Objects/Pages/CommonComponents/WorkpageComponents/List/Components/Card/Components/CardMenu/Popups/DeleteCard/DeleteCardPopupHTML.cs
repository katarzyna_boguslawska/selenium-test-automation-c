﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard
{
    public sealed class DeleteCardPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _deleteCardLocator = new Xpath("input").Descendant().WithClasses("js-confirm full negate").WithAttribute("type", "submit").Combined();
        readonly Xpath _backArrowLocator = new Xpath("a").Descendant().WithClasses("class='pop-over-header-back-btn icon-sm icon-back is-shown").Combined();
        readonly IWebDriver _driver;

        public DeleteCardPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement DeleteCardButton()
        {
            return GetElement(By.XPath(_deleteCardLocator.CompleteXpath));
        }

        public PageElement BackArrowButton()
        {
            return GetElement(By.XPath(_backArrowLocator.CompleteXpath));
        }
    }
}
