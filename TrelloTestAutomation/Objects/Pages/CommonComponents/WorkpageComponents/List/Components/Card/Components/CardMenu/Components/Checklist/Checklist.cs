﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using System.Collections.Generic;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist
{
    public class Checklist : BasePage
    {
        readonly IWebDriver _driver;
        public string Title { get; set; }
        public List<ChecklistItem> Items { get; set; }
        readonly ChecklistHTML _html;

        public Checklist(IWebDriver driver, string title) : base(driver)
        {
            _driver = driver;
            Title = title;
            Items = new List<ChecklistItem>();
            _html = new ChecklistHTML(driver, Title);
        }

        public int CountItems()
        {
            return _html.Items().Count;
        }

        private ChecklistItem MakeItem(string content)
        {
            _html.ItemContentField().FocusAndType(content);
            _html.AddItemButton().Click();
            _html.QuitAddingItemsButton().Click();
            return new ChecklistItem(_driver, content, Title);
        }

        public void AddItem(string content)
        {
            ChecklistItem item = MakeItem(content);
            Items.Add(item);
        }

        public ChecklistItem GetItemByContent(string content)
        {
            foreach (ChecklistItem item in Items)
            {
                if (item.Text.Equals(content))
                {
                    return item;
                }
            }
            throw new NotFoundException(string.Format("A checklist item with the text {0} was not found", content));
        }

        public void RemoveChecklist()
        {
            _html.DeleteChecklistButton().Click();
            _html.DeleteButton().Click();
            _html.CloseDeletionPopupButton().Click();
        }

        public bool HasTitle(string title)
        {
            try
            {
                string actualTitle = _html.Header().Text;
                return actualTitle.Equals(title) ? true : false;
            } catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsProgressAsExpected(double numberOfCompletedTasks)
        {
            SlowDown(500);
            double completionPercentage = (numberOfCompletedTasks / Items.Count) * 100.0f;
            int rounded = Convert.ToInt32(completionPercentage);
            string displayedProgress = _html.ProgressBar().Text.Substring(0, 2);
            int displayedNumericProgress = Int32.Parse(displayedProgress);
            return rounded == displayedNumericProgress;
        }

        public void CompleteTask(string whichTask)
        {
            GetItemByContent(whichTask).MarkAsDone();
        }
    }
}
