﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member
{
    public class MembersPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly MembersPopupHTML _html;
        readonly string _member;

        public MembersPopup(IWebDriver driver, string member)
        {
            _driver = driver;
            _html = new MembersPopupHTML(driver, member);
            _member = member;
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public void SearchAndSelect()
        {
            _html.MemberInput().Type(_member).ClickEnter();
            Close();
        }

        private void ChooseMemberFromList()
        {
            _html.MemberOption().Click();
        }

        public void AddMember()
        {
            ChooseMemberFromList();
            Close();
        }
    }
}
