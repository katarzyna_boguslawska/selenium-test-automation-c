﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label
{
    public sealed class LabelsPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _labelInputLocator = new Xpath("input").Descendant().WithClasses("js-autofocus js-label-search").Combined();
        readonly Xpath _createNewLabelLocator = new Xpath("a").Descendant().WithClasses("quiet-button full js-add-label").Combined();
        readonly Xpath _colorBlindModeLocator = new Xpath("a").Descendant().WithClasses("quiet-button full js-toggle-color-blind-mode").Combined();
        readonly Xpath _selectedLabelTickLocator = new Xpath("span").Descendant().WithClasses("icon-sm icon-check card-label-selectable-icon light").Combined();
        readonly Xpath _editLabelLocator = new Xpath("a").Descendant().WithClasses("card-label-edit-button icon-sm icon-edit js-edit-label").Combined();
        readonly IWebDriver _driver;

        public LabelsPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement LabelInput()
        {
            return GetElement(By.XPath(_labelInputLocator.CompleteXpath));
        }

        public PageElement CreateNewLabelButton()
        {
            return GetElement(By.XPath(_createNewLabelLocator.CompleteXpath));
        }

        public PageElement ColorBlindModeLink()
        {
            return GetElement(By.XPath(_colorBlindModeLocator.CompleteXpath));
        }

        public PageElement SelectedLabelTick()
        {
            return GetElement(By.XPath(_selectedLabelTickLocator.CompleteXpath));
        }

        public PageElement EditLabelButton()
        {
            return GetElement(By.XPath(_editLabelLocator.CompleteXpath));
        }

        public PageElement Label(Color color)
        {
            return GetElement(By.XPath(color.LabelLocator.CompleteXpath));
        }
    }
}
