﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components
{
    public class ChecklistItem : BasePage
    {
        readonly IWebDriver _driver;
        public string Text { get; set; }
        readonly string _checklistTitle;
        readonly ChecklistItemHTML _html;

        public ChecklistItem(IWebDriver driver, string text, string checklistTitle) : base(driver)
        {
            _driver = driver;
            Text = text;
            _checklistTitle = checklistTitle;
            _html = new ChecklistItemHTML(driver, Text, _checklistTitle);
        }

        public void MarkAsDone()
        {
            SlowDown(500);
            _html.DoneCheckbox().Click();
        }
    }
}
