﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist
{
    public class ChecklistPopup : ICloseable
    {
        readonly IWebDriver _driver;
        private ChecklistPopupHTML _html;

        public ChecklistPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new ChecklistPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        private void CompleteTitle(String title)
        {
            _html.ChecklistTitleInput().Type(title);
        }

        public Components.Checklist.Checklist AddChecklist(String title)
        {
            CompleteTitle(title);
            _html.AddChecklistButton().Click();
            return new Components.Checklist.Checklist(_driver, title);
        }
    }
}
