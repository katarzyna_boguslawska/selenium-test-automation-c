﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment
{
    public class AttachmentHTML : BasePage
    {
        readonly Xpath _attachmentContainer;
        readonly Xpath _commentOnAttachmentLink;
        readonly Xpath _deleteAttachmentLink;
        readonly Xpath _downloadAttachmentLink;
        readonly Xpath _removeCoverLink;
        readonly Xpath _makeCoverLink;
        readonly string _linkName;
        readonly IWebDriver _driver;

        public AttachmentHTML(IWebDriver driver, string linkName) : base(driver)
        {
            _driver = driver;
            _linkName = linkName;
            var containerAntecedent = new Xpath("div").Descendant().WithClass("window").Combined();
            var containerDescendant = new Xpath("a").Descendant().WithAttributeContaining("title", _linkName).Combined();
            _attachmentContainer = new Xpath("div").Descendant().WithClass("attachment-thumbnail").HavingAntecedent(containerAntecedent).HavingDescendant(containerDescendant).Combined();
            _commentOnAttachmentLink = new Xpath("a").Descendant().WithClasses("attachment-thumbnail-details-title-options-item js-reply").HavingAntecedent(_attachmentContainer).Combined();
            _deleteAttachmentLink = new Xpath("a").Descendant().WithClasses("attachment-thumbnail-details-title-options-item dark-hover js-confirm-delete").HavingAntecedent(_attachmentContainer).Combined();
            _downloadAttachmentLink = new Xpath("a").Descendant().WithClasses("attachment-thumbnail-details-options-item dark-hover js-download").HavingAntecedent(_attachmentContainer).Combined();
            _removeCoverLink = new Xpath("a").Descendant().WithClasses("attachment-thumbnail-details-options-item dark-hover js-remove-cover").HavingAntecedent(_attachmentContainer).Combined();
            _makeCoverLink = new Xpath("a").Descendant().WithClasses("attachment-thumbnail-details-options-item dark-hover hide js-make-cover").HavingAntecedent(_attachmentContainer).Combined();
        }

        public PageElement AttachmentContainer()
        {
            return GetElement(By.XPath(_attachmentContainer.CompleteXpath));
        }

        public PageElement CommentLink()
        {
            return GetElement(By.XPath(_commentOnAttachmentLink.CompleteXpath));
        }

        public PageElement DeleteLink()
        {
            return GetElement(By.XPath(_deleteAttachmentLink.CompleteXpath));
        }

        public PageElement DownloadLink()
        {
            return GetElement(By.XPath(_downloadAttachmentLink.CompleteXpath));
        }

        public PageElement RemoveCoverLink()
        {
            return GetElement(By.XPath(_removeCoverLink.CompleteXpath));
        }

        public PageElement MakeCoverLink()
        {
            return GetElement(By.XPath(_makeCoverLink.CompleteXpath));
        }
    }
}
