﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment
{
    public class NewComment
    {
        readonly IWebDriver _driver;
        public string Text { get; set; }
        readonly Emoji _emoji;
        readonly NewCommentHTML _html;

        public NewComment(IWebDriver driver, string text)
        {
            _driver = driver;
            Text = text;
            _html = new NewCommentHTML(_driver);
        }

        public NewComment(IWebDriver driver, string text, Emoji emoji)
        {
            _driver = driver;
            Text = text;
            _html = new NewCommentHTML(_driver);
            _emoji = emoji;
        }

        public PostedComment Comment()
        {
            _html.CommentTextarea().FocusAndType(Text);
            if (_emoji != null){
                EmojiPopup popup = OpenEmojiPopup();
                popup.InsertEmoji(_emoji);
            }
            _html.CommentSubmitButton().Click();
            return new PostedComment(_driver, Text);
        }

        private EmojiPopup OpenEmojiPopup()
        {
            _html.EmojiButton().Click();
            return new EmojiPopup(_driver);
        }

        public PostedComment CommentWithEmoji()
        {
            _html.CommentTextarea().FocusAndType(Text);
            EmojiPopup popup = OpenEmojiPopup();
            popup.InsertEmoji(_emoji);
            _html.CommentSubmitButton();
            return new PostedComment(_driver, Text);
        }
    }
}
