﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteComment
{
    public class DeleteCommentPopup : ICloseable, IDeletable
    {
        readonly IWebDriver _driver;
        readonly DeleteCommentPopupHTML _html;

        public DeleteCommentPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new DeleteCommentPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public virtual void Delete()
        {
            _html.DeleteButton().Click();
        }
    }
}
