﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment
{
    public class NewCommentHTML : BasePage
    {
        static readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClass("window").Combined();
        readonly Xpath _commentLocator = new Xpath("textarea").Descendant().WithClasses("comment-box-input js-new-comment-input").HavingAntecedent(_windowLocator).Combined();
        readonly Xpath _emojiLocator = new Xpath("a").Descendant().WithClasses("comment-box-options-item js-comment-add-emoji").HavingAntecedent(_windowLocator).Combined();
        readonly Xpath _attachmentLocator = new Xpath("a").Descendant().WithClasses("comment-box-options-item js-comment-add-attachment").HavingAntecedent(_windowLocator).Combined();
        readonly Xpath _mentionLocator = new Xpath("a").Descendant().WithClasses("comment-box-options-item js-comment-mention-member").HavingAntecedent(_windowLocator).Combined();
        readonly Xpath _cardLocator = new Xpath("a").Descendant().WithClasses("comment-box-options-item js-comment-add-card").HavingAntecedent(_windowLocator).Combined();
        readonly Xpath _submitCommentLocator = new Xpath("input").Descendant().WithClasses("primary confirm mod-no-top-bottom-margin js-add-comment").HavingAntecedent(_windowLocator).Combined();
        readonly IWebDriver _driver;

        public NewCommentHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CommentTextarea()
        {
            return GetElement(By.XPath(_commentLocator.CompleteXpath));
        }

        public PageElement CommentSubmitButton()
        {
            return GetElement(By.XPath(_submitCommentLocator.CompleteXpath));
        }

        public PageElement EmojiButton()
        {
            return GetElement(By.XPath(_emojiLocator.CompleteXpath));
        }

        public PageElement AttachmentButton()
        {
            return GetElement(By.XPath(_attachmentLocator.CompleteXpath));
        }

        public PageElement MentionButton()
        {
            return GetElement(By.XPath(_mentionLocator.CompleteXpath));
        }

        public PageElement CardButton()
        {
            return GetElement(By.XPath(_cardLocator.CompleteXpath));
        }
    }
}
