﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card
{
    public sealed class CardHTML : BasePage
    {
        readonly Xpath _cardMenuLocator;
        readonly Xpath _futureDueDateLocator;
        readonly Xpath _imageLocator;
        readonly Xpath _commentBadgeLocator;
        readonly Xpath _cardContainer;
        readonly Xpath _greenLabelLocator;
        readonly Xpath _yellowLabelLocator;
        readonly Xpath _orangeLabelLocator;
        readonly Xpath _redLabelLocator;
        readonly Xpath _purpleLabelLocator;
        readonly Xpath _blueLabelLocator;
        readonly string _text;
        readonly IWebDriver _driver;

        public CardHTML(IWebDriver driver, string textOnCard) : base(driver)
        {
            _driver = driver;
            _text = textOnCard;
            var descendant = new Xpath("*").Descendant().WithExactText(textOnCard).Combined();
            _cardContainer = new Xpath("a").Descendant().WithClass("list-card js-member-droppable").HavingDescendant(descendant).Combined();

            _cardMenuLocator = new Xpath("a").Descendant().WithClasses("list-card-title js-card-name").HavingAntecedent(_cardContainer).Combined();
            _futureDueDateLocator = new Xpath("div").Descendant().WithClasses("badge is-due-future").HavingAntecedent(_cardContainer).Combined();
            _imageLocator = new Xpath("div").Descendant().WithClasses("list-card-cover js-card-cover").HavingAntecedent(_cardContainer).Combined();
            _commentBadgeLocator = new Xpath("div").Descendant().WithClass("badge").WithAttribute("title", "Comments").HavingAntecedent(_cardContainer).Combined();
            _greenLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.green.CheckLabelLocator);
            _yellowLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.yellow.CheckLabelLocator);
            _orangeLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.orange.CheckLabelLocator);
            _redLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.red.CheckLabelLocator);
            _purpleLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.purple.CheckLabelLocator);
            _blueLabelLocator = new Xpath("").HavingAntecedent(_cardContainer, Color.blue.CheckLabelLocator);
        }

        public PageElement MemberOption(string who)
        {
            var memberXpath = new Xpath("span").Descendant().WithClass("member-initials").WithAttributeContaining("title", who).HavingAntecedent(_cardContainer).Combined();
            return GetElement(By.XPath(memberXpath.CompleteXpath));
        }

        public PageElement CardMenuLink()
        {
            return GetElement(By.XPath(_cardMenuLocator.CompleteXpath));
        }

        public PageElement FutureDueDateBadge()
        {
            return GetElement(By.XPath(_futureDueDateLocator.CompleteXpath));
        }

        public PageElement ImageBadge()
        {
            SlowDown(200);
            return GetElement(By.XPath(_imageLocator.CompleteXpath));
        }

        public PageElement CommentBadge()
        {
            return GetElement(By.XPath(_commentBadgeLocator.CompleteXpath));
        }

        public PageElement CardContainer()
        {
            return GetElement(By.XPath(_cardContainer.CompleteXpath));
        }

        public bool IsLabelPresent(Color color)
        {
            if (color.Equals(Color.green))
            {
                return IsPresent(By.XPath(_greenLabelLocator.CompleteXpath));
            }
            else if (color.Equals(Color.yellow))
            {
                return IsPresent(By.XPath(_yellowLabelLocator.CompleteXpath));
            }
            else if (color.Equals(Color.orange))
            {
                return IsPresent(By.XPath(_orangeLabelLocator.CompleteXpath));
            }
            else if (color.Equals(Color.red))
            {
                return IsPresent(By.XPath(_redLabelLocator.CompleteXpath));
            }
            else if (color.Equals(Color.purple))
            {
                return IsPresent(By.XPath(_purpleLabelLocator.CompleteXpath));
            }
            else
            {
                return IsPresent(By.XPath(_blueLabelLocator.CompleteXpath));
            }

        }
    }
}
