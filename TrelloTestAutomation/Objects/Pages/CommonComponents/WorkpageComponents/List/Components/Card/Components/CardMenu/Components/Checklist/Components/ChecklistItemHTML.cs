﻿using OpenQA.Selenium;
using System;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist.Components
{
    public sealed class ChecklistItemHTML : BasePage
    {
        readonly Xpath _itemContainer;
        readonly Xpath _doneCheckbox;
        readonly IWebDriver _driver;
        string _text;
        string _checklistTitle;

        public ChecklistItemHTML(IWebDriver driver, string itemText, string checklistTitle) : base(driver)
        {
            _driver = driver;
            _text = itemText;
            _checklistTitle = checklistTitle;
            var windowXpath = new Xpath("div").Descendant().WithClass("window").Combined();
            var headerXpath = new Xpath("h3").Descendant().WithExactText(checklistTitle).Combined();
            var checklist = new Xpath("div").Descendant().WithExactClass("checklist").HavingAntecedent(windowXpath).HavingDescendant(headerXpath).Combined();
            var textXpath = new Xpath("span").Descendant().WithExactText(itemText).Combined();
            _itemContainer = new Xpath("div").Descendant().HavingDescendant(textXpath).WithExactClass("checklist-item").HavingAntecedent(checklist).Combined();
            _doneCheckbox = new Xpath("span").Descendant().WithClasses("icon-sm icon-check checklist-item-checkbox-check").HavingAntecedent(_itemContainer).Combined();
        }

        public PageElement DoneCheckbox()
        {
            Console.WriteLine(_doneCheckbox.CompleteXpath);
            return GetElement(By.XPath(_doneCheckbox.CompleteXpath));
        }

        public PageElement ItemContainer()
        {
            return GetElement(By.XPath(_itemContainer.CompleteXpath));
        }
    }
}
