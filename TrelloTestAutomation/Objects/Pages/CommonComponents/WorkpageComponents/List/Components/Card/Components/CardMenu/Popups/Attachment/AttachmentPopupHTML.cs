﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment
{
    public sealed class AttachmentPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClass("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClass("pop-over is-shown").Combined();
        readonly Xpath _linkInputLocator = new Xpath("input").Descendant().WithAttribute("id", "addLink").Combined();
        readonly Xpath _addLinkLocator = new Xpath("input").Descendant().WithClass("js-add-attachment-url").Combined();
        readonly IWebDriver _driver;

        public AttachmentPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement LinkInput()
        {
            return GetElement(By.XPath(_linkInputLocator.CompleteXpath));
        }

        public PageElement AddLinkButton()
        {
            return GetElement(By.XPath(_addLinkLocator.CompleteXpath));
        }
    }
}
