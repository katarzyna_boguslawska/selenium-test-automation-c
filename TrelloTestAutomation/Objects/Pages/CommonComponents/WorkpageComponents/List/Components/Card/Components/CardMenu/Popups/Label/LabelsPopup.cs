﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.FixedChoices;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label
{
    public class LabelsPopup : ICloseable
    {
        private IWebDriver _driver;
        private LabelsPopupHTML _html;

        public LabelsPopup(IWebDriver driver)
        {
            _driver = driver;
            this._html = new LabelsPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public void SearchAndSelect(string searchWhat)
        {
            _html.LabelInput().Type(searchWhat).ClickEnter();
            Close();
        }

        public void Label(Color color)
        {
            _html.Label(color).Click();
            Close();
        }

        public void Delabel()
        {
            _html.SelectedLabelTick().Click();
        }
    }
}
