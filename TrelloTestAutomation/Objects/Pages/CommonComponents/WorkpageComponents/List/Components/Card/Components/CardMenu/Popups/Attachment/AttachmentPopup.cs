﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment
{ 
    public class AttachmentPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly AttachmentPopupHTML _html;

        public AttachmentPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new AttachmentPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public Components.Attachment.Attachment AddAttachment(string link)
        {
            _html.LinkInput().Type(link);
            _html.AddLinkButton().Click();
            return new Components.Attachment.Attachment(_driver, link);
        }
    }
}
