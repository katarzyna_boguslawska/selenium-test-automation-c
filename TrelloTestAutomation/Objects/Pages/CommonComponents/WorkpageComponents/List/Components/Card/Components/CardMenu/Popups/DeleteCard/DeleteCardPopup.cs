﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DeleteCard
{
    public class DeleteCardPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly DeleteCardPopupHTML _html;

        public DeleteCardPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new DeleteCardPopupHTML(_driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }

        public ShareAndMorePopup GoBackToPopup(ShareAndMorePopup popup)
        {
            _html.BackArrowButton().Click();
            return popup;
        }

        public void DeleteCard()
        {
            _html.DeleteCardButton().Click();
        }

    }
}
