﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Member;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Label;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Attachment;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Checklist;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu
{
    public class CardMenu
    {
        readonly IWebDriver _driver;
        readonly Card _card;
        private List<Checklist> _checklists;
        private List<Attachment> _attachments;
        private List<PostedComment> _comments;
        NewComment NewComment;
        readonly CardMenuHTML _html;

        public CardMenu(IWebDriver driver, Card card)
        {
            _driver = driver;
            _card = card;
            _checklists = new List<Checklist>();
            _attachments = new List<Attachment>();
            _comments = new List<PostedComment>();
            _html = new CardMenuHTML(_driver);
        }

        public void HideMenu()
        {
            try
            {
                _html.CloseButton().Click();
            } catch(TimeoutException)
            {
                _html.CloseButtonOnCover().Click();
            }
        }

        private MembersPopup OpenMemberPopup(string member)
        {
            _html.MembersButton().Click();
            return new MembersPopup(_driver, member);
        }

        private LabelsPopup OpenLabelPopup()
        {
            _html.LabelsButton().Click();
            return new LabelsPopup(_driver);
        }

        private AttachmentPopup OpenAttachmentPopup()
        {
            _html.AttachmentButton().Click();
            return new AttachmentPopup(_driver);
        }

        private ChecklistPopup OpenChecklistPopup()
        {
            _html.ChecklistButton().Click();
            return new ChecklistPopup(_driver);
        }

        private DueDatePopup OpenDueDatePopup()
        {
            _html.DueDateButton().Click();
            return new DueDatePopup(_driver);
        }

        private ShareAndMorePopup OpenShareAndMorePopup()
        {
            _html.ShareAndMoreLink().Click();
            return new ShareAndMorePopup(_driver);
        }

        private EmojiPopup OpenEmojiPopup()
        {
            _html.EmojiButton().Click();
            return new EmojiPopup(_driver);
        }
        public void AddDueDate(string date)
        {
            DueDatePopup popup = OpenDueDatePopup();
            popup.AddDueDate(date);
            HideMenu();
        }
        public void ArchiveCard()
        {
            _html.ArchiveButton().Click();
            HideMenu();
        }

        private Attachment MakeAttachmentFromLink(string link)
        {
            AttachmentPopup popup = OpenAttachmentPopup();
            return popup.AddAttachment(link);
        }

        public void AddAttachmentFromLink(string link)
        {
            Attachment attachment = MakeAttachmentFromLink(link);
            _attachments.Add(attachment);
            HideMenu();
        }

        public JsonExportedPage.JsonExportedPage ExportToJson()
        {
            ShareAndMorePopup popup = OpenShareAndMorePopup();
            return popup.ExportToJson(_card);
        }

        private Checklist MakeChecklist(string title)
        {
            ChecklistPopup popup = OpenChecklistPopup();
            return popup.AddChecklist(title);
        }

        public void AddChecklist(string title)
        {
            Checklist checklist = MakeChecklist(title);
            _checklists.Add(checklist);
        }

        public void RemoveChecklist(string checklistTitle)
        {
            Checklist checklist = FindChecklistByTitle(checklistTitle);
            checklist.RemoveChecklist();
        }

        public void PopulateChecklist(string checklistTitle, string taskName, int tasksAmount = 4)
        {
            Checklist checklist = FindChecklistByTitle(checklistTitle);
            for (int i = 0; i < tasksAmount; i++)
            {
                string text = taskName + (i + 1);
                checklist.AddItem(text);
            }
        }

        public int CountChecklists()
        {
            return _html.Checklists().Count;
        }

        public Checklist FindChecklistByTitle(string title)
        {
            foreach (Checklist checklist in _checklists)
            {
                if (checklist.Title.Equals(title))
                {
                    return checklist;
                }
            }
            throw new NoSuchElementException(string.Format("Checklist entitled {0} was not found", title));
        }

        public void Label(Color color)
        {
            LabelsPopup popup = OpenLabelPopup();
            popup.Label(color);
            HideMenu();
        }
        public void AddMember(string who)
        {
            MembersPopup popup = OpenMemberPopup(who);
            popup.AddMember();
            HideMenu();
        }

        private PostedComment MakeComment(string commentText)
        {
            NewComment = new NewComment(_driver, commentText);
            return NewComment.Comment();
        }

        private PostedComment MakeComment(string commentText, Emoji emoji)
        {
            NewComment = new NewComment(_driver, commentText, emoji);
            return NewComment.Comment();
        }

        public void Comment(string commentText)
        {
            PostedComment comment = MakeComment(commentText);
            _comments.Add(comment);
        }

        public void Comment(string commentText, Emoji emoji)
        {
            PostedComment comment = MakeComment(commentText, emoji);
            _comments.Add(comment);
        }

        public PostedComment FindCommentByText(string commentText)
        {
            foreach (PostedComment comment in _comments)
            {
                if (comment.Text.Equals(commentText))
                {
                    return comment;
                }
            }
            throw new NoSuchElementException(string.Format("Comment '{0}' was not found", commentText));
        }

        public PostedComment FindCommentByText(string commentText, bool tolerance)
        {
            foreach (PostedComment comment in _comments)
            {
                if (comment.Text.StartsWith(commentText))
                {
                    return comment;
                }
            }
            throw new NoSuchElementException(string.Format("Comment '{0}' was not found", commentText));
        }

        public void DeleteCommentWithContent(string commentText)
        {
            PostedComment toDelete = FindCommentByText(commentText);
            toDelete.Delete();
            _comments.Remove(toDelete);
            HideMenu();
        }

        public bool IsCommentRemoved(string commentText)
        {
            try
            {
                PostedComment comment = FindCommentByText(commentText);
                bool isAbsent = (comment.GetCommentText() == null);
                HideMenu();
                return isAbsent;
            } catch (NoSuchElementException)
            {
                return true;
            }
        }

        public bool WasChecklistAdded(string title)
        {
            return FindChecklistByTitle(title) != null ? true : false;
        }

        public bool WasChecklistWithTitlePopulated(string checklistTitle, int tasksNumber)
        {
            Checklist checklist = FindChecklistByTitle(checklistTitle);
            return checklist.CountItems() == tasksNumber ? true : false;
        }

        public bool WereTasksCompleted(string checklistTitle, double howManyTasks)
        {
            Checklist checklist = FindChecklistByTitle(checklistTitle);
            return checklist.IsProgressAsExpected(howManyTasks);
        }

        public bool WasCommentAdded(string commentText)
        {
            PostedComment comment = FindCommentByText(commentText);
            bool isPresent = (comment.GetCommentText() != null);
            HideMenu();
            return isPresent;
        }
    }
}
