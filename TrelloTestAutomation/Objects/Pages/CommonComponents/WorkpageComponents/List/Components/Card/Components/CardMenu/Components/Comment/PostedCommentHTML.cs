﻿using System;
using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment
{
    public sealed class PostedCommentHTML : BasePage
    {
        readonly Xpath _commentContainer;
        readonly Xpath _editLocator;
        readonly Xpath _deleteLocator;
        string _text;
        readonly IWebDriver _driver;

        public PostedCommentHTML(IWebDriver driver, string commentText) : base(driver)
        {
            _driver = driver;
            _text = commentText;
            var antecedentXpath = new Xpath("div").Descendant().WithClass("window").Combined();
            var descendantXpath = new Xpath("p").Descendant().WithExactText(_text).Combined();
            _commentContainer = new Xpath("div").Descendant().WithClasses("phenom mod-comment-type").HavingDescendant(descendantXpath).HavingAntecedent(antecedentXpath).Combined();
            _editLocator = new Xpath("a").Descendant().WithClass("js-edit-action").HavingAntecedent(_commentContainer).Combined();
            _deleteLocator = new Xpath("a").Descendant().WithClass("js-confirm-delete-action").HavingAntecedent(_commentContainer).Combined();
        }

        public PageElement PostedComment()
        {
            return GetElement(By.XPath(_commentContainer.CompleteXpath));
        }

        public PageElement EditButton()
        {
            return GetElement(By.XPath(_editLocator.CompleteXpath));
        }

        public PageElement DeleteLink()
        {
            return GetElement(By.XPath(_deleteLocator.CompleteXpath));
        }

        public PageElement EmojiImage(Emoji emoji)
        {
            return GetElement(By.XPath(emoji.EmojiImage.CompleteXpath));
        }
    }
}
