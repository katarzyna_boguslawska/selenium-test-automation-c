﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.ShareAndMore
{
    public sealed class ShareAndMorePopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _jsonExportLocator = new Xpath("a").Descendant().WithClass("js-export-json").Combined();
        readonly Xpath _deleteLocator = new Xpath("a").Descendant().WithClass("js-delete").Combined();
        readonly IWebDriver _driver;

        public ShareAndMorePopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
        
        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement JsonExportOption()
        {
            return GetElement(By.XPath(_jsonExportLocator.CompleteXpath));
        }

        public PageElement DeleteLink()
        {
            return GetElement(By.XPath(_deleteLocator.CompleteXpath));
        }
    }
}
