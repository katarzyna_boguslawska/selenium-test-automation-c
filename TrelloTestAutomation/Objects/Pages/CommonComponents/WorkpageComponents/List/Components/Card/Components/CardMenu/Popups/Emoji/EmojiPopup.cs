﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji
{
    public class EmojiPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly EmojiPopupHTML _html;

        public EmojiPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new EmojiPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseButton().ClickEnter();
        }

        public void InsertEmoji(FixedChoices.Emoji emoji)
        {
            _html.Emoji(emoji).Click();
        }
    }
}
