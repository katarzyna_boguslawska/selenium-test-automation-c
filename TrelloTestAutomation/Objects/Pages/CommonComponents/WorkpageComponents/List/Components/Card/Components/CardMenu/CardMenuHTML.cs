﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using System.Collections.Generic;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Comment;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Attachment;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu
{
    public sealed class CardMenuHTML : BasePage
    {
        readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClass("window").Combined();
        readonly Xpath _membersLocator = new Xpath("a").Descendant().WithClasses("button-link js-change-card-members").Combined();
        readonly Xpath _labelsLocator = new Xpath("a").Descendant().WithClasses("button-link js-edit-labels").Combined();
        readonly Xpath _checklistLocator = new Xpath("a").Descendant().WithClass("button-link js-add-checklist-menu").Combined();
        readonly Xpath _dueDateLocator = new Xpath("a").Descendant().WithClass("button-link js-add-due-date").Combined();
        readonly Xpath _attachmentLocator = new Xpath("a").Descendant().WithClass("button-link js-attach").Combined();
        readonly Xpath _archiveLocator = new Xpath("a").Descendant().WithClass("button-link js-archive-card").Combined();
        readonly Xpath _shareAndMoreLocator = new Xpath("a").Descendant().WithClasses("quiet-button js-more-menu").Combined();
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("icon-lg icon-close dialog-close-button js-close-window").Combined();
        readonly Xpath _closeLocatorOnCover = new Xpath("a").Descendant().WithClass("icon-lg icon-close dialog-close-button js-close-window dialog-close-button-light").Combined();
        readonly Xpath _genericChecklistLocator = new Xpath("div").Descendant().WithClass("checklist").Combined();
        readonly IWebDriver _driver;
        readonly NewCommentHTML _newCommentSectionHTML;
        public List<Dictionary<string, ChecklistHTML>> ChecklistHTMLs;
        public List<Dictionary<string, PostedCommentHTML>> CommentHTMLs;
        public List<Dictionary<string, AttachmentHTML>> AttachmentHTMLs;

        public CardMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _newCommentSectionHTML = new NewCommentHTML(driver);
            ChecklistHTMLs = new List<Dictionary<string, ChecklistHTML>>();
            CommentHTMLs = new List<Dictionary<string, PostedCommentHTML>>();
            AttachmentHTMLs = new List<Dictionary<string, AttachmentHTML>>();
        }

        public PageElement Window()
        {
            return GetElement(By.XPath(_windowLocator.CompleteXpath));
        }

        public PageElement MembersButton()
        {
            return GetElement(By.XPath(_membersLocator.CompleteXpath));
        }

        public PageElement LabelsButton()
        {
            return GetElement(By.XPath(_labelsLocator.CompleteXpath));
        }

        public PageElement ChecklistButton()
        {
            return GetElement(By.XPath(_checklistLocator.CompleteXpath));
        }

        public PageElement DueDateButton()
        {
            return GetElement(By.XPath(_dueDateLocator.CompleteXpath));
        }

        public PageElement AttachmentButton()
        {
            return GetElement(By.XPath(_attachmentLocator.CompleteXpath));
        }

        public PageElement ArchiveButton()
        {
            return GetElement(By.XPath(_archiveLocator.CompleteXpath));
        }

        public PageElement ShareAndMoreLink()
        {
            return GetElement(By.XPath(_shareAndMoreLocator.CompleteXpath));
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement CloseButtonOnCover()
        {
            return GetElement(By.XPath(_closeLocatorOnCover.CompleteXpath));
        }

        public List<PageElement> Checklists()
        {
            return GetElements(By.XPath(_genericChecklistLocator.CompleteXpath));
        }

        public PageElement CommentTextarea()
        {
            return _newCommentSectionHTML.CommentTextarea();
        }

        public PageElement EmojiButton()
        {
            return _newCommentSectionHTML.EmojiButton();
        }

        public PageElement CommentAttachmentButton()
        {
            return _newCommentSectionHTML.AttachmentButton();
        }

        public PageElement CommentCardButton()
        {
            return _newCommentSectionHTML.CardButton();
        }

        public PageElement MentionButton()
        {
            return _newCommentSectionHTML.MentionButton();
        }

        public PageElement SubmitCommentButton()
        {
            return _newCommentSectionHTML.CommentSubmitButton();
        }

        public ChecklistHTML ChecklistHTML(string checklistTitle)
        {
            foreach (Dictionary<string, ChecklistHTML> html in ChecklistHTMLs)
            {
                if (html.ContainsKey(checklistTitle))
                {
                    return html[checklistTitle];
                }
            }
            throw new NotFoundException(string.Format("A checklist titled {0} was not found.", checklistTitle));
        }

        public PostedCommentHTML CommentHTML(string commentText)
        {
            foreach (Dictionary<string, PostedCommentHTML> html in CommentHTMLs)
            {
                if (html.ContainsKey(commentText))
                {
                    return html[commentText];
                }
            }
            throw new NotFoundException(string.Format("A comment with the content {0} was not found.", commentText));
        }

        public AttachmentHTML AttachmentHTML(string attachmentLink)
        {
            foreach (Dictionary<string, AttachmentHTML> html in AttachmentHTMLs)
            {
                if (html.ContainsKey(attachmentLink))
                {
                    return html[attachmentLink];
                }
            }
            throw new NotFoundException(string.Format("An attachment linking to {0} was not found.", attachmentLink));
        }
    }
}
