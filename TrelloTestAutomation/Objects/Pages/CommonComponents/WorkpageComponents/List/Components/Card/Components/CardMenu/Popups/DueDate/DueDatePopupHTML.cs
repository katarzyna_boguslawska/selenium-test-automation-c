﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.DueDate
{
    public sealed class DueDatePopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-clos").Combined();
        readonly Xpath _windowLocator = new Xpath("div").Descendant().WithClasses("pop-over is-shown").Combined();
        readonly Xpath _dateInputLocator = new Xpath("input").Descendant().WithClasses("datepicker-select-input js-dpicker-date-input js-autofocus").Combined();
        readonly Xpath _addDueDateLocator = new Xpath("input").Descendant().WithClasses("primary wide confirm").WithAttribute("type", "submit").Combined();
        readonly Xpath _removeDueDateLocator = new Xpath("button").Descendant().WithClasses("negate remove-date js-remove-date").Combined();
        readonly IWebDriver _driver;

        public DueDatePopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement DateInput()
        {
            return GetElement(By.XPath(_dateInputLocator.CompleteXpath));
        }

        public PageElement AddDueDateButton()
        {
            return GetElement(By.XPath(_addDueDateLocator.CompleteXpath));
        }

        public PageElement RemoveDueDateButton()
        {
            return GetElement(By.XPath(_removeDueDateLocator.CompleteXpath));
        }
    }
}
