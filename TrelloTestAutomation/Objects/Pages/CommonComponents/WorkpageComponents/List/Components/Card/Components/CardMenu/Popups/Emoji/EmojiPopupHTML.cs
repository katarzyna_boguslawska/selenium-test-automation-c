﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Popups.Emoji
{
    public sealed class EmojiPopupHTML : BasePage
    {
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _searchEmojiLocator = new Xpath("input").Descendant().WithClasses("js-filter-emoji js-autofocus").Combined();
        readonly IWebDriver _driver;

        public EmojiPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement SearchEmojiInput()
        {
            return GetElement(By.XPath(_searchEmojiLocator.CompleteXpath));
        }

        public PageElement Emoji(FixedChoices.Emoji emoji)
        {
            return GetElement(By.XPath(emoji.EmojiLocator.CompleteXpath));
        }
    }
}
