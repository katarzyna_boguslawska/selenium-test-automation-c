﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using System.Collections.Generic;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu.Components.Checklist;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List.Components.Card.Components.CardMenu;
using System;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List
{
    public class List
    {
        public string Title { get; set; }
        public List<Card> Cards { get; set; }
        readonly IWebDriver _driver;
        readonly ListHTML _html;

        public List(string title, IWebDriver driver)
        {
            _driver = driver;
            Title = title;
            Cards = new List<Card>();
            _html = new ListHTML(driver, Title);
        }

        public void Archive()
        {
            _html.MoreMenuButton().Click();
        }

        private PageElement GetCardContainer(string textOnCard)
        {
            return _html.CardContainer(textOnCard);
        }


        private Card MakeCard(string textOnCard)
        {
            _html.AddCardButton().Click();
            _html.CardTextButton().Type(textOnCard);
            _html.AddButton().Click();
            _html.QuitAddingButton().Click();
            PageElement parent = GetCardContainer(textOnCard);
            return new Card(_driver, textOnCard);
        }

        public void AddCard(string textOnCard)
        {
            Card card = MakeCard(textOnCard);
            Cards.Add(card);
        }

        public Card FindCardByText(string cardText)
        {
            foreach (Card card in Cards)
            {
                if (card.Text.Equals(cardText))
                {
                    return card;
                }
            }
            return null;
        }

        public void AddDueDateOnCardWithText(string cardText, string date)
        {
            FindCardByText(cardText).ShowCardMenu().AddDueDate(date);
        }

        public void AddAttachmentFromLinkToCardWithText(string cardText, string link)
        {
            FindCardByText(cardText).ShowCardMenu().AddAttachmentFromLink(link);
        }

        public void ArchiveCardWithText(string cardText)
        {
            Card card = FindCardByText(cardText);
            card.ShowCardMenu().ArchiveCard();
            Cards.Remove(card);
        }

        public JsonExportedPage.JsonExportedPage ExportToJsonCardWithText(string cardText)
        {
            return FindCardByText(cardText).ShowCardMenu().ExportToJson();
        }

        public void AddChecklistToCardWithText(string cardText, string checklistTitle)
        {
            FindCardByText(cardText).ShowCardMenu().AddChecklist(checklistTitle);
        }

        public void AddChecklistToCardWithText(string cardText, string checklistTitle, string taskName)
        {
            FindCardByText(cardText).ShowCardMenu().AddChecklist(checklistTitle);
            FindCardByText(cardText).ShowCardMenu(true).PopulateChecklist(checklistTitle, taskName);
        }

        public void CompleteListedTasksOnCardWithText(string cardText, string checklistTitle, string[] tasks)
        {
            Checklist checklist = FindCardByText(cardText).Menu.FindChecklistByTitle(checklistTitle);
            foreach (string task in tasks)
            {
                checklist.CompleteTask(task);
            }
        }

        public void DragCardWithTextInPlaceOfCardWithText(string sourceText, string destinationText, List destinationList)
        {
            Card destination = destinationList.FindCardByText(destinationText);
            FindCardByText(sourceText).DragAndDropCard(destination);
            destinationList.Cards.Add(FindCardByText(sourceText));
            Cards.Remove(FindCardByText(sourceText));
        }

        public void LabelCardWithText(string cardText, Color color)
        {
            FindCardByText(cardText).ShowCardMenu().Label(color);
        }

        public void AssignMemberToCardWithText(string cardText, string who)
        {
            FindCardByText(cardText).ShowCardMenu().AddMember(who);
        }

        public void CommentCardWithText(string cardText, string commentText)
        {
            FindCardByText(cardText).ShowCardMenu().Comment(commentText);
        }

        public void CommentCardWithTextAndAddEmoji(string cardText, string commentText, Emoji emoji)
        {
            FindCardByText(cardText).ShowCardMenu().Comment(commentText, emoji);
        }

        public void DeleteCommentFromCardWithText(string cardText, string commentText)
        {
            FindCardByText(cardText).ShowCardMenu().DeleteCommentWithContent(commentText);
        }

        public bool WasDueDateAddedToCardWithText(string cardText)
        {
            return FindCardByText(cardText).DueDateWasAdded();
        }

        public bool WasCardWithTextArchived(string cardText)
        {
            return FindCardByText(cardText) != null ? false : true;
        }

        public bool WasAttachmentAddedToCardWithText(string cardText)
        {
            return FindCardByText(cardText).AttachmentWasAdded();
        }

        public bool WasCardWithTextDroppedInPlaceOfCardWithText(string sourceCardText, List destinationList)
        {
            Card dragged;
            try
            {
                dragged = FindCardByText(sourceCardText);
            }
            catch (StaleElementReferenceException)
            {
                dragged = null;
            }
            PageElement droppedParent = destinationList._html.ListContainer();
            Card dropped = destinationList.FindCardByText(sourceCardText);
            return (dragged == null) && (dropped != null) ? true : false;
        }

        public bool WasCardWithTextLabelled(string cardText, Color color)
        {
            return FindCardByText(cardText).HasLabelOfColor(color);
        }

        public bool WasCardWithTextAssignedTo(string cardText, string member)
        {
            return FindCardByText(cardText).MemberWasAdded(member);
        }

        public bool WasCardWithTextCommented(string cardText, string commentText)
        {
            Card card = FindCardByText(cardText);
            CardMenu menu = card.ShowCardMenu(true);
            return menu.WasCommentAdded(commentText);
        }

        public bool WasCardWithTextCommented(string cardText, string commentText, Emoji emoji)
        {
            Card card = FindCardByText(cardText);
            CardMenu menu = card.ShowCardMenu(true);
            bool isEmoji = menu.FindCommentByText(commentText, true).CheckIfHasEmoji(emoji);
            menu.HideMenu();
            return isEmoji;
        }

        public bool WasCommentRemovedFromCardWithText(string cardText, string commentText)
        {
            CardMenu menu = FindCardByText(cardText).ShowCardMenu();
            return menu.IsCommentRemoved(commentText);
        }

        public bool WasChecklistAddedToCardWithText(string cardText, string checklistTitle)
        {
            Card card = FindCardByText(cardText);
            bool wasAdded = card.ShowCardMenu(true).WasChecklistAdded(checklistTitle);
            card.ShowCardMenu(true).RemoveChecklist(checklistTitle);
            return wasAdded;
        }

        public bool WasChecklistPopulatedOnCardWithText(string cardText, string checklistTitle)
        {
            Card card = FindCardByText(cardText);
            bool wasPopulated = card.ShowCardMenu(true).WasChecklistWithTitlePopulated(checklistTitle, 4);
            card.ShowCardMenu(true).RemoveChecklist(checklistTitle);
            return wasPopulated;
        }

        public bool WereTasksCompleted(string cardText, string checklistTitle, int howManyWereCompleted)
        {
            double completedTasksNumber = (double)howManyWereCompleted;
            Card card = FindCardByText(cardText);
            bool wereCompleted = card.ShowCardMenu(true).WereTasksCompleted(checklistTitle, completedTasksNumber);
            card.ShowCardMenu(true).RemoveChecklist(checklistTitle);
            return wereCompleted;
        }
    }
}
