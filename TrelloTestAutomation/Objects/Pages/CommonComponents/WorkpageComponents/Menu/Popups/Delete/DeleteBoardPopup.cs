﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete
{
    public class DeleteBoardPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly DeleteBoardPopupHTML _html;

        public DeleteBoardPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new DeleteBoardPopupHTML(driver);
        }

        public virtual void Close()
        {
            _html.ClosePopupButton().Click();
        }

        public DeletedWorkPagePage.DeletedWorkPagePage PermanentlyDelete()
        {
            _html.PermanentlyDeleteButton().Click();
            return new DeletedWorkPagePage.DeletedWorkPagePage(_driver);
        }
    }
}
