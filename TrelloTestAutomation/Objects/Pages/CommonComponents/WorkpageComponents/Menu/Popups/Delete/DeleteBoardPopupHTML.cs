﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Delete
{
    public sealed class DeleteBoardPopupHTML : BasePage
    {
        readonly Xpath _closePopupLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _permanentlyDeleteLocator = new Xpath("input").Descendant().WithClasses("js-confirm full negate").WithAttribute("type", "submit").Combined();
        readonly IWebDriver _driver;

        public DeleteBoardPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement ClosePopupButton()
        {
            return GetElement(By.XPath(_closePopupLocator.CompleteXpath));
        }

        public PageElement PermanentlyDeleteButton()
        {
            return GetElement(By.XPath(_permanentlyDeleteLocator.CompleteXpath));
        }
    }
}
