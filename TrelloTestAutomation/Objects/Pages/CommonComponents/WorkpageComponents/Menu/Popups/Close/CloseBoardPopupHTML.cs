﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close
{
    public sealed class CloseBoardPopupHTML : BasePage
    {
        readonly Xpath _closeBoardButtonLocator = new Xpath("input").Descendant().WithClasses("js-confirm full negate").WithAttribute("type", "submit").Combined();
        readonly Xpath _closePopupLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly IWebDriver _driver;

        public CloseBoardPopupHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CloseBoardButton()
        {
            return GetElement(By.XPath(_closeBoardButtonLocator.CompleteXpath));
        }
        public PageElement ClosePopupButton() { return GetElement(By.XPath(_closePopupLocator.CompleteXpath)); }
    }
}
