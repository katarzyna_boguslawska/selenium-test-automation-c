﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close
{
    public class CloseBoardPopup : ICloseable
    {
        readonly IWebDriver _driver;
        readonly CloseBoardPopupHTML _html;

        public CloseBoardPopup(IWebDriver driver)
        {
            _driver = driver;
            _html = new CloseBoardPopupHTML(driver);
        }

        private DeleteBoardConfirmationPage.DeleteBoardConfirmationPage CloseBoard()
        {
            _html.CloseBoardButton().Click();
            return new DeleteBoardConfirmationPage.DeleteBoardConfirmationPage(_driver);
        }

        public DeletedWorkPagePage.DeletedWorkPagePage DeleteBoard()
        {
            return CloseBoard().ConfirmClosing();
        }

        public virtual void Close()
        {
            _html.ClosePopupButton().Click();
        }
    }
}
