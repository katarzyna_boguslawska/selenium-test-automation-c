﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu
{
    public class BoardMenu : GenericMenu, ICloseable
    {
        readonly IWebDriver _driver;
        readonly BoardMenuHTML _html;

        public BoardMenu(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new BoardMenuHTML(driver);
        }

        public new void Close()
        {
            _html.CloseMenuButton().Click();
        }

        private GenericMenu ReturnMenu(BoardMenuSubmenu submenu)
        {
            if (Object.Equals(submenu, BoardMenuSubmenu.More))
            {
                return new MoreMenu(_driver);
            }
            else if (Object.Equals(submenu, BoardMenuSubmenu.Background))
            {
                return new BackgroundMenus(_driver);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public GenericMenu ShowMenu(BoardMenuSubmenu menu)
        {
            _html.Submenu(menu).IsClickable();
            _html.Submenu(menu).Click();
            return ReturnMenu(menu);
        }

        public DeletedWorkPagePage.DeletedWorkPagePage DeleteBoard()
        {
            MoreMenu menu = (MoreMenu)ShowMenu(BoardMenuSubmenu.More);
            return menu.DeleteBoard();
        }

        public void ChangeBackgroundToColor(Color color)
        {
            BackgroundMenus menu = (BackgroundMenus)ShowMenu(BoardMenuSubmenu.Background);
            menu.ChangeBackgroundToColor(color);
        }

        public void ChangeBackgroundToRandomPhoto()
        {
            BackgroundMenus menu = (BackgroundMenus)ShowMenu(BoardMenuSubmenu.Background);
            menu.ChangeBackgroundToPhoto();

        }
    }
}
