﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu
{
    public sealed class BoardMenuHTML : BasePage
    {
        readonly Xpath _archivedItemsLocator = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-archive").Combined();
        readonly Xpath _closeMenuLocator = new Xpath("a").Descendant().WithClasses("board-menu-header-close-button icon-lg icon-close js-hide-sidebar").Combined();

        readonly IWebDriver _driver;

        public BoardMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
                
        public PageElement ArchivedItemsButton()
        {
            return GetElement(By.XPath(_archivedItemsLocator.CompleteXpath));
        }

        public PageElement CloseMenuButton()
        {
            return GetElement(By.XPath(_closeMenuLocator.CompleteXpath));
        }

        public PageElement Submenu(BoardMenuSubmenu submenu)
        {
            return GetElement(By.XPath(submenu.SubmenuPositionLocator.CompleteXpath));
        }
    }
}
