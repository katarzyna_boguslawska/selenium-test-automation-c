﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using System.Collections.Generic;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus
{
    public sealed class PhotoBackgroundMenuHTML : BasePage
    {
        readonly Xpath _genericPhotoLocator = new Xpath("div").Descendant().WithClass("board-background-select").Combined();
        readonly IWebDriver _driver;

        public PhotoBackgroundMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
        
        public List<PageElement> GenericPhoto()
        {
            return GetElements(By.XPath(_genericPhotoLocator.CompleteXpath));
        }
    }
}
