﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus
{
    public class ColorBackgroundMenu : GenericMenu
    {
        readonly ColorBackgroundMenuHTML _html;
        readonly IWebDriver _driver;

        public ColorBackgroundMenu(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new ColorBackgroundMenuHTML(driver);
        }

        public void ChooseColor(Color color)
        {
            _html.ColorTile(color).Click();
        }
    }
}
