﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus
{
    public class PhotoBackgroundMenu : GenericMenu
    {
        readonly PhotoBackgroundMenuHTML _html;

        public PhotoBackgroundMenu(IWebDriver driver) : base(driver)
        {
            _html = new PhotoBackgroundMenuHTML(driver);
        }

        private int GetRandomIndex()
        {
            Random random = new Random();
            return random.Next(0, 12);
        }

        public void ChooseRandomPhoto()
        {
            int index = GetRandomIndex();
            _html.GenericPhoto()[index].Click();
        }
    }
}
