﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background
{
    public sealed class BackgroundMenuHTML : BasePage
    {
        readonly Xpath _colorBackground = new Xpath("div").Descendant().WithClasses("board-backgrounds-section-tile board-backgrounds-colors-tile js-bg-colors").Combined();
        readonly Xpath _photoBackground = new Xpath("div").Descendant().WithClasses("board-backgrounds-section-tile board-backgrounds-photos-tile js-bg-photos").Combined();
        readonly IWebDriver _driver;

        public BackgroundMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
                
        public PageElement ColorBackgroundTile()
        {
            return GetElement(By.XPath(_colorBackground.CompleteXpath));
        }

        public PageElement PhotoBackgroundTile()
        {
            return GetElement(By.XPath(_photoBackground.CompleteXpath));
        }
    }
}
