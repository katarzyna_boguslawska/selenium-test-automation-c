﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus
{
    public class ColorBackgroundMenuHTML :BasePage
    {
        readonly IWebDriver _driver;

        public ColorBackgroundMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement ColorTile(Color color)
        {
            return GetElement(By.XPath(color.BackgroundLocator.CompleteXpath));
        }
    }
}
