﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic
{
    public sealed class GenericMenuHTML : BasePage
    {
        readonly Xpath _closeMenuLocator = new Xpath("a").Descendant().WithClasses("board-menu-header-close-button icon-lg icon-close js-hide-sidebar").Combined();
        readonly Xpath _backArrowMenuButton = new Xpath("a").Descendant().WithClasses("board-menu-header-back-button icon-lg icon-back js-pop-widget-view").Combined();
        readonly Xpath _moreLocator = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-more").Combined();
        readonly IWebDriver _driver;

        public GenericMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
        
        public PageElement CloseMenuButton()
        {
            return GetElement(By.XPath(_closeMenuLocator.CompleteXpath));
        }

        public PageElement BackArrowMenuButton()
        {
            return GetElement(By.XPath(_backArrowMenuButton.CompleteXpath));
        }

        public PageElement MoreButton()
        {
            return GetElement(By.XPath(_moreLocator.CompleteXpath));
        }
    }
}
