﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic
{
    public class GenericMenu : ICloseable
    {
        readonly IWebDriver _driver;
        readonly GenericMenuHTML _html;

        public GenericMenu(IWebDriver driver)
        {
            _driver = driver;
            _html = new GenericMenuHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseMenuButton().Click();
        }

        public BoardMenu ReturnToMainMenu()
        {
            while (!(_html.IsPresent(_html.MoreButton)))
            {
                _html.BackArrowMenuButton().Click();
            }
            return new BoardMenu(_driver);
        }
    }
}
