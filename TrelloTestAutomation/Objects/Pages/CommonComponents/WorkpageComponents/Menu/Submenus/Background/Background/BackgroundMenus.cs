﻿using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Submenus;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Background
{
    public class BackgroundMenus : GenericMenu
    {
        private IWebDriver _driver;
        private BackgroundMenuHTML _html;



        public BackgroundMenus(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new BackgroundMenuHTML(driver);
        }

        public ColorBackgroundMenu ChooseColorMenu()
        {
            _html.ColorBackgroundTile().Click();
            return new ColorBackgroundMenu(_driver);
        }

        public PhotoBackgroundMenu ChoosePhotoMenu()
        {
            _html.PhotoBackgroundTile().Click();
            return new PhotoBackgroundMenu(_driver);
        }

        public void ChangeBackgroundToColor(Color color)
        {
            ColorBackgroundMenu colorMenu = ChooseColorMenu();
            colorMenu.ChooseColor(color);
        }

        public void ChangeBackgroundToPhoto()
        {
            PhotoBackgroundMenu photoMenu = ChoosePhotoMenu();
            photoMenu.ChooseRandomPhoto();
        }
    }
}
