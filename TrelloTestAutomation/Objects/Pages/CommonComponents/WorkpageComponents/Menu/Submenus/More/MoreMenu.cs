﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.Background.Generic;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Popups.Close;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More
{
    public class MoreMenu : GenericMenu
    {
        readonly IWebDriver _driver;
        readonly MoreMenuHTML _html;

        public MoreMenu(IWebDriver driver) : base(driver)
        {
            _driver = driver;
            _html = new MoreMenuHTML(driver);
        }

        private CloseBoardPopup CloseBoard()
        {
            _html.CloseBoardButton().Click();
            return new CloseBoardPopup(_driver);
        }

        public DeletedWorkPagePage.DeletedWorkPagePage DeleteBoard()
        {
            return CloseBoard().DeleteBoard();
        }
    }
}
