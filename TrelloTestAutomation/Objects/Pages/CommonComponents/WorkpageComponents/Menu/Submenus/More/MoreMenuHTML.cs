﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu.Submenus.More
{
    public sealed class MoreMenuHTML : BasePage
    {
        readonly Xpath _closeBoardLocator = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-close-board").Combined();
        readonly IWebDriver _driver;

        public MoreMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
                
        public PageElement CloseBoardButton()
        {
            return GetElement(By.XPath(_closeBoardLocator.CompleteXpath));
        }
    }
}
