﻿using System.Collections.Generic;
using OpenQA.Selenium;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu;
using TrelloTestAutomation.Objects.Pages.CommonHTML;

namespace TrelloTestAutomation.Objects.Pages.GroupWorkPage
{
    public class GroupWorkPage : LoggedInPage.LoggedInPage
    {
        public List<List> Lists { get; set; }
        readonly IWebDriver _driver;
        public BoardMenu Menu { get; set; }
        public string Title { get; set; }
        private string _workspace;
        private WorkPageHTML _html;

        public GroupWorkPage(IWebDriver driver, string title, string workspace) : base(driver)
        {
            _driver = driver;
            Title = title;
            Lists = new List<List>();
            _workspace = workspace;
            _html = new WorkPageHTML(driver, title);
        }

        private List MakeList(string listTitle)
        {
            _html.ListTitleInput().Type(listTitle);
            _html.SaveListButton().Click();
            return new List(listTitle, _driver);
        }

        public void AddList(string listTitle)
        {
            List list = MakeList(listTitle);
            Lists.Add(list);
        }

        public List FindListByTitle(string listTitle)
        {
            foreach (List list in Lists)
            {
                if (list.Title.Equals(listTitle))
                {
                    return list;
                }
            }
            return null;
        }

        public void ArchiveListWithTitle(string listTitle)
        {
            List listToArchive = FindListByTitle(listTitle);
            listToArchive.Archive();
            Lists.Remove(listToArchive);
        }

        public int CountLists()
        {
            return _html.GenericList().Count;
        }

        public BoardMenu ShowBoardMenu()
        {
            if (IsPresent(_html.CloseMenuButton) == false)
            {
                _html.ShowMenuLink().Click();
                Menu = new BoardMenu(_driver);
                return Menu;
            }
            if (Menu != null)
            {
                return Menu;
            }
            else
            {
                Menu = new BoardMenu(_driver);
                return Menu;
            }
        }

        public DeletedWorkPagePage.DeletedWorkPagePage DeleteBoard()
        {
            BoardMenu menu = ShowBoardMenu();
            return menu.DeleteBoard();
        }

        public void RenameBoard(string newName)
        {
            _html.TitleLink().Click();
            _html.NewNameInput().Type(newName);
            _html.NameChangeButton().Click();
            _html.BoardName = newName;
        }

        public bool IsTitled(string name)
        {
            try
            {
                _html.WorkPageTitle();
                return true;
            } catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsBackgroundColored(Color color)
        {
            string backgroundColor = _html.Body().GetCssValue("background-color");
            return backgroundColor.Equals(color.RGBcomponents);
        }

        public bool IsBackgroundImage()
        {
            string backgroundImage = GetBackgroundPattern();
            return backgroundImage.StartsWith("url(\"https");
        }

        private string GetBackgroundPattern()
        {
            int maxAttempt = (int)(5 / 0.5);
            int attempt = 0;
            SlowDown(500);
            while (attempt < maxAttempt)
            {
                if (_html.Body().GetCssValue("background-image").Equals("none"))
                {
                    attempt++;
                    SlowDown(500);
                }
                else
                {
                    return _html.Body().GetCssValue("background-image");
                }
            }
            return "none";
        }

    }
}
