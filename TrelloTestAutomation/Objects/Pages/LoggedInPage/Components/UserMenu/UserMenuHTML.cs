﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.UserMenu
{
    public sealed class UserMenuHTML : BasePage
    {
        readonly Xpath _profileLocator = new Xpath("a").Descendant().WithClass("js-profile").Combined();
        readonly Xpath _cardsLocator = new Xpath("a").Descendant().WithClass("js-cards").Combined();
        readonly Xpath _settingsLocator = new Xpath("a").Descendant().WithClass("js-account").Combined();
        readonly Xpath _helpLocator = new Xpath("a").Descendant().WithClass("js-help").Combined();
        readonly Xpath _shortcutsLocator = new Xpath("a").Descendant().WithClass("js-shortcuts").Combined();
        readonly Xpath _changeLanguageLocator = new Xpath("a").Descendant().WithClass("js-change-locale").Combined();
        readonly Xpath _logoutLocator = new Xpath("a").Descendant().WithClass("js-logout").Combined();
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly IWebDriver _driver;

        public UserMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        

        public PageElement ProfileOption()
        {
            return GetElement(By.XPath(_profileLocator.CompleteXpath));
        }

        public PageElement CardsOption()
        {
            return GetElement(By.XPath(_cardsLocator.CompleteXpath));
        }

        public PageElement SettingsOption()
        {
            return GetElement(By.XPath(_settingsLocator.CompleteXpath));
        }

        public PageElement HelpOption()
        {
            return GetElement(By.XPath(_helpLocator.CompleteXpath));
        }

        public PageElement ShortcutsOption()
        {
            return GetElement(By.XPath(_shortcutsLocator.CompleteXpath));
        }

        public PageElement ChangeLanguageOption()
        {
            return GetElement(By.XPath(_changeLanguageLocator.CompleteXpath));
        }

        public PageElement LogoutOption()
        {
            return GetElement(By.XPath(_logoutLocator.CompleteXpath));
        }

        public PageElement CloseOption()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }
    }
}
