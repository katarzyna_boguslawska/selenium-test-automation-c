﻿using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.UserMenu
{
    public class UserMenu : ICloseable
    {
        private IWebDriver _driver;
        private UserMenuHTML _html;

        public UserMenu(IWebDriver driver)
        {
            _driver = driver;
            this._html = new UserMenuHTML(driver);
        }

        public virtual void Close()
        {
            _html.CloseOption().Click();
        }

        public void Logout()
        {
            _html.LogoutOption().Click();
        }
    }
}
