﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.BusinessTeamProfilePage;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups
{
    public class BusinessTeamCreator : ICreatorable<BusinessTeamProfile>, ICloseable
    {
        readonly IWebDriver _driver;
        readonly CreatorHTML _html;

        public BusinessTeamCreator(IWebDriver driver)
        {
            _driver = driver;
            _html = new CreatorHTML(_driver);
        }

        public virtual void CompleteName(String name)
        {
            _html.GroupNameInput().Type(name);
        }

        public virtual void CompleteDescription(String description)
        {
            _html.DescriptionInput().Type(description);
        }

        public virtual BusinessTeamProfile ClickCreate(String name)
        {
            _html.CreateButton().Click();
            return new BusinessTeamProfile(_driver, name);
        }

        public virtual BusinessTeamProfile CreateTeam(String name)
        {
            CompleteName(name);
            return ClickCreate(name);
        }

        public virtual BusinessTeamProfile CreateTeam(String name, string description)
        {
            CompleteName(name);
            CompleteDescription(description);
            return ClickCreate(name);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }
    }
}
