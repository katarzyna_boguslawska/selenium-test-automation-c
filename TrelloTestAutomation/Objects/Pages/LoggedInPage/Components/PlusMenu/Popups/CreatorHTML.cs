﻿using System;
using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups
{
    public sealed class CreatorHTML : BasePage
    {
        readonly Xpath _groupNameLocator = new Xpath("input").Descendant().WithAttribute("id", "org-display-name").Combined();
        readonly Xpath _descriptionLocator = new Xpath("textarea").Descendant().WithAttribute("id", "org-desc").Combined();
        readonly Xpath _createButtonLocator = new Xpath("input").Descendant().WithClasses("primary wide js-save").WithAttribute("value", "Create").Combined();
        readonly Xpath _closeLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-close-btn icon-sm icon-close").Combined();
        readonly Xpath _backLocator = new Xpath("a").Descendant().WithClasses("pop-over-header-back-btn icon-sm icon-back is-shown").Combined();
        readonly IWebDriver _driver;

        public CreatorHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }
                
        public PageElement GroupNameInput()
        {
            return GetElement(By.XPath(_groupNameLocator.CompleteXpath));
        }

        public PageElement DescriptionInput()
        {
            return GetElement(By.XPath(_descriptionLocator.CompleteXpath));
        }

        public PageElement CreateButton()
        {
            return GetElement(By.XPath(_createButtonLocator.CompleteXpath));
        }

        public PageElement CloseButton()
        {
            return GetElement(By.XPath(_closeLocator.CompleteXpath));
        }

        public PageElement BackButton()
        {
            return GetElement(By.XPath(_backLocator.CompleteXpath)); 
        }
    }
}
