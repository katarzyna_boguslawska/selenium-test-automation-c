﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Interfaces;
using TrelloTestAutomation.Objects.Pages.PersonalTeamProfilePage;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups
{
    public class PersonalTeamCreator : ICloseable, ICreatorable<PersonalTeamProfile>
    {
        readonly IWebDriver _driver;
        readonly CreatorHTML _html;

        public PersonalTeamCreator(IWebDriver driver)
        {
            _driver = driver;
            _html = new CreatorHTML(_driver);
        }

        public virtual void CompleteName(String name)
        {
            _html.GroupNameInput().Type(name);
        }

        public virtual void CompleteDescription(String description)
        {
            _html.DescriptionInput().Type(description);
        }

        public virtual PersonalTeamProfile ClickCreate(String name)
        {
            _html.CreateButton().Click();
            return new PersonalTeamProfile(_driver, name);
        }

        public virtual PersonalTeamProfile CreateTeam(String name)
        {
            CompleteName(name);
            return ClickCreate(name);
        }

        public virtual PersonalTeamProfile CreateTeam(String name, string description)
        {
            CompleteName(name);
            CompleteDescription(description);
            return ClickCreate(name);
        }

        public virtual void Close()
        {
            _html.CloseButton().Click();
        }
    }
}
