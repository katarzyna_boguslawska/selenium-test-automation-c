﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu
{
    public sealed class PlusMenuHTML : BasePage
    {
        readonly Xpath _createBoardLocator = new Xpath("a").Descendant().WithClass("js-new-board").Combined();
        readonly Xpath _createPersonalTeamLocator = new Xpath("a").Descendant().WithClass("js-new-org").Combined();
        readonly Xpath _createBusinessTeamLocator = new Xpath("a").Descendant().WithClass("js-new-bc-org").Combined();
        readonly IWebDriver _driver;

        public PlusMenuHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement CreateBoardOption()
        {
            return GetElement(By.XPath(_createBoardLocator.CompleteXpath));
        }

        public PageElement CreatePersonalTeamOption()
        {
            return GetElement(By.XPath(_createPersonalTeamLocator.CompleteXpath));
        }

        public PageElement CreateBusinessTeamOption()
        {
            return GetElement(By.XPath(_createBusinessTeamLocator.CompleteXpath));
        }
    }
}
