﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu.Popups;
using TrelloTestAutomation.Objects.Pages.PersonalTeamProfilePage;
using TrelloTestAutomation.Objects.Pages.BusinessTeamProfilePage;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu
{
    public class PlusMenu
    {
        readonly IWebDriver _driver;
        readonly PlusMenuHTML _html;

        public PlusMenu(IWebDriver driver)
        {
            _driver = driver;
            this._html = new PlusMenuHTML(_driver);
        }

        private PersonalTeamCreator OpenPersonalTeamCreationMenu()
        {
            _html.CreatePersonalTeamOption().Click();
            return new PersonalTeamCreator(_driver);
        }

        private BusinessTeamCreator OpenBusinessTeamCreationMenu()
        {
            _html.CreateBusinessTeamOption().Click();
            return new BusinessTeamCreator(_driver);
        }

        public PersonalTeamProfile CreatePersonalTeam(string teamName)
        {
            PersonalTeamCreator creator = OpenPersonalTeamCreationMenu();
            return creator.CreateTeam(teamName);
        }

        public PersonalTeamProfile CreatePersonalTeam(string teamName, string description)
        {
            PersonalTeamCreator creator = OpenPersonalTeamCreationMenu();
            return creator.CreateTeam(teamName, description);
        }

        public BusinessTeamProfile CreateBusinessTeam(string name)
        {
            BusinessTeamCreator creator = OpenBusinessTeamCreationMenu();
            return creator.CreateTeam(name);
        }

        public BusinessTeamProfile CreateBusinessTeam(string name, string description)
        {
            BusinessTeamCreator creator = OpenBusinessTeamCreationMenu();
            return creator.CreateTeam(name, description);
        }
    }
}
