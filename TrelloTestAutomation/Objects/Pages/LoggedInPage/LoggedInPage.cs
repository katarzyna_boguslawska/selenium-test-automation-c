﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;
using TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.PlusMenu;
using TrelloTestAutomation.Objects.Pages.LoggedInPage.Components.UserMenu;
using TrelloTestAutomation.Objects.Pages.PersonalTeamProfilePage;
using TrelloTestAutomation.Objects.Pages.BusinessTeamProfilePage;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage
{
    public class LoggedInPage : BasePage
    {
        private IWebDriver _driver;
        public List<GroupWorkPage.GroupWorkPage> GroupBoards { get; set; }
        public List<PersonalWorkPage.PersonalWorkPage> PersonalBoards { get; set; }
        private LoggedInPageHTML _html;

        public LoggedInPage(IWebDriver driver) : base(driver)
        {

            _driver = driver;
            GroupBoards = new List<GroupWorkPage.GroupWorkPage>();
            PersonalBoards = new List<PersonalWorkPage.PersonalWorkPage>();
            _html = new LoggedInPageHTML(_driver);
        }

        public bool IsCreateBoardPossible()
        {
            return _html.CreateNewBoardButton().IsVisible();
        }

        private GroupWorkPage.GroupWorkPage MakeNewGroupBoard(string groupName, string boardTitle)
        {
            _html.NewBoardTile(boardTitle, groupName).Click();
            _html.BoardTitleInput().Type(boardTitle);
            _html.CreateBoardButton().Click();
            return new GroupWorkPage.GroupWorkPage(_driver, boardTitle, groupName);
        }

        public void AddNewGroupBoard(string groupName, string boardTitle)
        {
            GroupWorkPage.GroupWorkPage groupWorkPage = MakeNewGroupBoard(groupName, boardTitle);
            GroupBoards.Add(groupWorkPage);
        }

        private PersonalWorkPage.PersonalWorkPage MakeNewPersonalBoard(string boardTitle)
        {
            _html.NewBoardTile(boardTitle, "Personal Boards").Click();
            _html.BoardTitleInput().Type(boardTitle);
            _html.CreateBoardButton().Click();
            return new PersonalWorkPage.PersonalWorkPage(_driver, boardTitle, "Personal Boards");
        }

        public void AddNewPersonalBoard(String boardTitle)
        {
            PersonalWorkPage.PersonalWorkPage personalWorkPage = MakeNewPersonalBoard(boardTitle);
            PersonalBoards.Add(personalWorkPage);
        }

        public GroupWorkPage.GroupWorkPage GoToWorkPage(string groupName, string boardTitle)
        {
            _html.BoardTile(boardTitle, groupName).Click();
            return new GroupWorkPage.GroupWorkPage(_driver, boardTitle, groupName);
        }

        public SearchResultsPage.SearchResultsPage SearchForBoard(string boardTitle)
        {
            _html.BoardSearchInput().FocusAndType(boardTitle);
            _html.SearchButton().Click();
            return new SearchResultsPage.SearchResultsPage(_driver, boardTitle);
        }


        public LoggedInPage GoToHomeDashboard()
        {
            _html.HomeDashboardButton().Click();
            return new LoggedInPage(_driver);
        }

        public PlusMenu ShowPlusMenu()
        {
            _html.PlusMenuButton().Click();
            return new PlusMenu(_driver);
        }

        public UserMenu ShowUserMenu()
        {
            _html.AvatarMenuButton().Click();
            return new UserMenu(_driver);
        }

        public PersonalTeamProfile CreatePersonalTeam(String groupName)
        {
            PlusMenu menu = ShowPlusMenu();
            return menu.CreatePersonalTeam(groupName);
        }

        public BusinessTeamProfile CreateBusinessTeam(String groupName)
        {
            PlusMenu menu = ShowPlusMenu();
            return menu.CreateBusinessTeam(groupName);
        }

        public void LogOut()
        {
            UserMenu menu = ShowUserMenu();
            menu.Logout();

        }
    }
}
