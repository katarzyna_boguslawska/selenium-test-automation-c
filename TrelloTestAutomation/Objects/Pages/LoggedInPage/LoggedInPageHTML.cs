﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.LoggedInPage
{
    public sealed class LoggedInPageHTML : BasePage
    {
        readonly Xpath _welcomeBoardLocator = new Xpath("a").Descendant().WithClass("board-tile").WithAttributeContaining("href", "welcome-board").Combined();
        readonly Xpath _createNewBoardLocator = new Xpath("a").Descendant().WithClasses("board-tile mod-add").Combined();
        readonly Xpath _boardTitleInputLocator = new Xpath("input").Descendant().WithId("boardNewTitle").Combined();
        readonly Xpath _createBoardButton = new Xpath("input").Descendant().WithClasses("primary wide js-submit").WithAttribute("type", "submit").WithAttribute("value", "Create").Combined();
        readonly Xpath _avatarMenuLocator = new Xpath("a").Descendant().WithClasses("header-btn header-avatar js-open-header-member-menu").Combined();
        readonly Xpath _boardSearchInputLocator = new Xpath("input").Descendant().WithClasses("header-search-input js-search-input js-disable-on-dialog").Combined();
        readonly Xpath _searchButtonLocator = new Xpath("a").Descendant().WithClasses("header-search-icon header-search-icon-open icon-lg icon-external-link light js-open-button js-open-search-page header-search-icon-dark").Combined();
        readonly Xpath _homeDashboardLocator = new Xpath("a").Descendant().WithClasses("header-logo js-home-via-logo").WithAttribute("aria-label", "Trello Home").Combined();
        readonly Xpath _plusMenuLocator = new Xpath("a").Descendant().WithClasses("header-btn js-open-add-menu").Combined();
        Xpath _boardTileLocator;

        IWebDriver _driver;

        public LoggedInPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement WelcomeBoardLabel()
        {
            return GetElement(By.XPath(_welcomeBoardLocator.CompleteXpath));
        }

        public PageElement CreateNewBoardButton()
        {
            return GetElement(By.XPath(_createNewBoardLocator.CompleteXpath));
        }

        public PageElement BoardTitleInput()
        {
            return GetElement(By.XPath(_boardTitleInputLocator.CompleteXpath));
        }

        public PageElement CreateBoardButton()
        {
            return GetElement(By.XPath(_createBoardButton.CompleteXpath));
        }

        public PageElement AvatarMenuButton()
        {
            return GetElement(By.XPath(_avatarMenuLocator.CompleteXpath));
        }

        public PageElement BoardSearchInput()
        {
            return GetElement(By.XPath(_boardSearchInputLocator.CompleteXpath));
        }

        public PageElement SearchButton()
        {
            return GetElement(By.XPath(_searchButtonLocator.CompleteXpath));
        }

        public PageElement HomeDashboardButton()
        {
            return GetElement(By.XPath(_homeDashboardLocator.CompleteXpath));
        }

        public PageElement PlusMenuButton()
        {
            return GetElement(By.XPath(_plusMenuLocator.CompleteXpath));
        }

        public PageElement BoardTile(string boardTitle, string groupName)
        {
            var antecedentXpath = new Xpath("h3").Descendant().WithClasses("boards-page-board-section-header-name").WithText(groupName).Parent().Parent().Combined();
            var descendantXpath = new Xpath("span").Descendant().WithAttribute("title", boardTitle).Combined();
            _boardTileLocator = new Xpath("a").Descendant().HavingAntecedent(antecedentXpath).WithClass("board-tile").HavingDescendant(descendantXpath).Combined();
            return GetElement(By.XPath(_boardTileLocator.CompleteXpath));
        }

        public PageElement NewBoardTile(string boardTitle, string groupName)
        {
            var antecedentXpath = new Xpath("h3").Descendant().WithClasses("boards-page-board-section-header-name").WithText(groupName).Grandparent().Combined();
            var descendantXpath = new Xpath("span").Descendant().WithText("Create new board").Combined();
            var boardTileLocator = new Xpath("a").Descendant().HavingAntecedent(antecedentXpath).WithClass("board-tile").HavingDescendant(descendantXpath).Combined();
            return GetElement(By.XPath(boardTileLocator.CompleteXpath));
        }
    }
}