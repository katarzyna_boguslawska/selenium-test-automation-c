﻿using TrelloTestAutomation.Objects.Base;
using OpenQA.Selenium;

namespace TrelloTestAutomation.Objects.Pages.FailedLoginPage
{
    public sealed class FailedLoginPageHTML : BasePage
    {
        readonly Xpath _tFailureLocator = new Xpath("p").Descendant().WithClass("error-message").Combined();
        readonly Xpath _failureLocator = new Xpath("div").Descendant().WithId("error").Combined();
        IWebDriver _driver;

        public FailedLoginPageHTML(IWebDriver driver) : base(driver)
        {
            _driver = driver;
        }

        public PageElement TFailure()
        {
            return GetElement(By.XPath(_tFailureLocator.CompleteXpath));
        }

        public PageElement Failure()
        {
            return GetElement(By.XPath(_failureLocator.CompleteXpath));
        }
    }
}
