﻿using System;
using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Objects.Pages.FailedLoginPage
{
    public class FailedLoginPage
    {
        readonly IWebDriver _driver;
        readonly FailedLoginPageHTML _html;

        public FailedLoginPage(IWebDriver driver)
        {
            _driver = driver;
            _html = new FailedLoginPageHTML(driver);
        }

        public bool IsErrorCommunicated()
        {
            try
            {
                PageElement errorMsg = _html.Failure();
                return true;
            }
            catch (TimeoutException)
            {
                try
                {
                    PageElement errorMsg = _html.TFailure();
                    return true;
                }
                catch (TimeoutException)
                {
                    return false;
                }
            }
        }
    }
}
