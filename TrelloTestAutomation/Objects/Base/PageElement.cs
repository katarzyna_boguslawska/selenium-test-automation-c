﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;
using System.Collections.ObjectModel;
using System.Drawing;

namespace TrelloTestAutomation.Objects.Base
{
    public class PageElement : IWebElement
    {
        private IWebElement _root;
        private By _locator;
        readonly IWebDriver _driver;

        public PageElement(By locator, IWebDriver driver)
        {
            this._locator = locator;
            _driver = driver;
        }

        public PageElement(By locator, IWebDriver driver, IWebElement element)
        {
            this._locator = locator;
            _driver = driver;
            this._root = element;
        }

        public By Locator
        {
            get { return _locator; }
            set { _locator = value; }
        }

        public IWebElement Root
        {
            get { return _root; }
            set { _root = value; }
        }

        public string TagName => _root.TagName;

        public string Text => _root.Text;

        public bool Enabled => _root.Enabled;

        public bool Selected => _root.Selected;

        public Point Location => _root.Location;

        public Size Size => _root.Size;

        public bool Displayed => _root.Displayed;

        public void Clear()
        {
            _root.Clear();
        }

        public void Click()
        {
            Actions action = new Actions(_driver);
            action.MoveToElement(_root).Click(_root).Perform();
        }

        public IWebElement FindElement(By by)
        {
            return _root.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _root.FindElements(by);
        }

        public string GetAttribute(string attributeName)
        {
            return _root.GetAttribute(attributeName);
        }

        public string GetCssValue(string propertyName)
        {
            return _root.GetCssValue(propertyName);
        }

        public void SendKeys(string text)
        {
            _root.SendKeys(text);
        }

        public void Submit()
        {
            _root.Submit();
        }

        public PageElement DragTo(IWebElement destination)
        {
            Actions action = new Actions(_driver);
            action.MoveToElement(_root).DragAndDrop(_root, destination).Perform();
            return this;
        }

        public PageElement DragTo(Xpath destination)
        {
            Actions action = new Actions(_driver);
            IWebElement destinationElement = _driver.FindElement(By.XPath(destination.CompleteXpath));
            action.MoveToElement(_root).DragAndDrop(_root, destinationElement).Perform();
            return this;
        }

        public PageElement FocusAndType(string text)
        {
            Clear();
            Actions action = new Actions(_driver);
            action.MoveToElement(_root).Click().SendKeys(text).Perform();
            return this;

        }

        public PageElement Type(string text)
        {
            Clear();
            Actions action = new Actions(_driver);
            action.MoveToElement(_root).SendKeys(text).Perform();
            return this;

        }

        public PageElement ClickEnter()
        {
            _root.SendKeys(Keys.Enter);
            return this;

        }

        public void Validate()
        {
            if (this == null)
            {
                throw new NoSuchElementException();
            }
        }

        public void GoBack()
        {
            _driver.Navigate().Back();
        }

        public PageElement ScrollTo(PageElement element)
        {
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", element.Root);
            return element;
        }

        public bool IsVisible()
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
                wait.Until(ExpectedConditions.ElementIsVisible(this.Locator));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsClickable()
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
                wait.Until(ExpectedConditions.ElementToBeClickable(this.Locator));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }
    }
}
