﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Objects.Base
{
    public class BasePage : ICheckable
    {
        private string _url;
        readonly IWebDriver _driver;

        public BasePage(IWebDriver driver)
        {
            _driver = driver;
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public void Navigate(string url)
        {
            _driver.Navigate().GoToUrl(url);
        }

        public bool IsPresent(By locator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
                wait.Until(ExpectedConditions.ElementExists(locator));
                return true;
            }
            catch(WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsPresent(Func<PageElement> toBeCalled)
        {
            try
            {
                toBeCalled();
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsVisible(Func<PageElement> toBeCalled)
        {
            try
            {
                PageElement element = toBeCalled();
                return element.IsVisible();
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsVisible(By locator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
                wait.Until(ExpectedConditions.ElementIsVisible(locator));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsClickable(Func<PageElement> toBeCalled)
        {
            try
            {
                PageElement elem = toBeCalled();
                return elem.IsClickable();
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsClickable(By locator)
        {
            try
            {
                WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
                wait.Until(ExpectedConditions.ElementToBeClickable(locator));
                return true;
            }
            catch (WebDriverTimeoutException)
            {
                return false;
            }
        }

        public bool IsAbsent(By locator)
        {
            return !(IsPresent(locator));
        }

        public void SlowDown(int miliseconds)
        {
            System.Threading.Thread.Sleep(miliseconds);
        }

        public bool IsAbsent(PageElement referenceElement, By locator)
        {
            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.5);
            int attempt = 0;
            SlowDown(500);
            while (attempt < maxAttempt)
            {
                if (referenceElement.FindElements(locator).Count == 0)
                {
                    return true;
                }
                else
                {
                    attempt++;
                    SlowDown(500);
                }
            }
            return false;
        }

        public bool IsAbsent(PageElement element)
        {
            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.5);
            int attempt = 0;
            SlowDown(500);
            while (attempt < maxAttempt)
            {
                if (_driver.FindElements(element.Locator).Count == 0)
                {
                    return true;
                }
                else
                {
                    attempt++;
                    SlowDown(500);
                }
            }
            return false;
        }

        public bool IsAbsent(By referenceElementLocator, By locator)
        {
            PageElement referenceElement = GetElement(referenceElementLocator);
            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.2);
            int attempt = 0;
            SlowDown(200);
            while (attempt < maxAttempt)
            {
                if (referenceElement.FindElements(locator).Count == 0)
                {
                    return true;
                }
                else
                {
                    attempt++;
                    SlowDown(200);
                }
            }
            return false;
        }

        public PageElement GetElement(By locator)
        {
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));
            IWebElement root = wait.Until(ExpectedConditions.ElementIsVisible(locator));
            return new PageElement(locator, _driver, root);
        }

        public PageElement GetElement(PageElement parent, By locator)
        {
            int maxAttempt = (int)(3 / 0.2);
            int attempt = 0;
            SlowDown(200);
            while (attempt < maxAttempt)
            {
                try
                {
                    IWebElement root = parent.FindElement(locator);
                    return new PageElement(locator, _driver, root);
                }
                catch (NoSuchElementException)
                {
                    attempt++;
                    SlowDown(200);
                }
            }
            throw new NoSuchElementException();

        }

        public PageElement GetElement(By parent, By locator)
        {
            PageElement parentElement = GetElement(parent);
            WebDriverWait wait = new WebDriverWait(_driver, TimeSpan.FromSeconds(testSettings.Default.GENERAL_TIMEOUT));

            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.2);
            int attempt = 0;
            SlowDown(200);
            while (attempt < maxAttempt)
            {
                try
                {
                    IWebElement root = parentElement.FindElement(locator);
                    return new PageElement(locator, _driver, root);
                }
                catch (NoSuchElementException)
                {
                    attempt++;
                    SlowDown(200);
                }
            }
            throw new NoSuchElementException();
        }

        public List<PageElement> GetElements(By locator)
        {
            if (IsPresent(locator))
            {
                List<PageElement> elements = new List<PageElement>();
                ReadOnlyCollection<IWebElement> webElements = _driver.FindElements(locator);
                foreach (IWebElement webElement in webElements)
                {
                    PageElement element = new PageElement(locator, _driver, webElement);
                    elements.Add(element);
                }
                return elements;
            }
            else
            {
                throw new NoSuchElementException();
            }
        }

        public List<PageElement> GetElements(PageElement referenceElement, By locator)
        {
            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.5);
            int attempt = 0;
            List<PageElement> elements = new List<PageElement>();
            SlowDown(500);
            while (attempt < maxAttempt)
            {
                ReadOnlyCollection<IWebElement> webElements = referenceElement.FindElements(locator);
                if (webElements.Count > 0)
                {
                    foreach (IWebElement webElement in webElements)
                    {
                        PageElement element = new PageElement(locator, _driver, webElement);
                        elements.Add(element);
                    }
                    return elements;
                }
                else
                {
                    attempt++;
                    SlowDown(500);
                }
            }
            throw new NoSuchElementException();
        }

        public List<PageElement> GetElements(By referenceElementLocator, By locator)
        {
            PageElement referenceElement = GetElement(referenceElementLocator);
            int maxAttempt = (int)(testSettings.Default.GENERAL_TIMEOUT / 0.5);
            int attempt = 0;
            List<PageElement> elements = new List<PageElement>();
            SlowDown(500);
            while (attempt < maxAttempt)
            {
                ReadOnlyCollection<IWebElement> webElements = referenceElement.FindElements(locator);
                if (webElements.Count > 0)
                {
                    foreach (IWebElement webElement in webElements)
                    {
                        PageElement element = new PageElement(locator, _driver, webElement);
                        elements.Add(element);
                    }
                    return elements;
                }
                else
                {
                    attempt++;
                    SlowDown(500);
                }
            }
            throw new NoSuchElementException();
        }

        public void ValidateElement(PageElement element)
        {
            if (element == null)
            {
                throw new NoSuchElementException();
            }
        }

        public void GoBack()
        {
            _driver.Navigate().Back();
        }

        public PageElement ScrollTo(By locator)
        {
            PageElement element = GetElement(locator);
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", element.Root);
            return element;
        }

        public PageElement ScrollTo(PageElement element)
        {
            ((IJavaScriptExecutor)_driver).ExecuteScript("arguments[0].scrollIntoView(true);", element.Root);
            return element;
        }
    }
}