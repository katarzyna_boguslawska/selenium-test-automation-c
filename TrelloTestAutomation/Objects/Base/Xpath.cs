﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TrelloTestAutomation.Objects.Base
{
    public class Xpath
    {
        private string _node;
        private string _id;
        private string _descendant;
        private string _child;
        private string _havingParent;
        private string _havingAntecedent;
        private string _withExactClass;
        private string _withClass;
        private string _withClasses;
        private string _withExactText;
        private string _withText;
        private string _withTextStarting;
        private List<Dictionary<string, string>> _withAttribute;
        private List<Dictionary<string, string>> _withAttributeContaining;
        private string _havingChild;
        private string _havingDescendant;
        private string _parent;
        public string CompleteXpath { get; set; }


        public Xpath(string node)
        {
            _node = node;
            CompleteXpath = string.Format("{0}[]", _node);
            _withAttribute = new List<Dictionary<string, string>>();
            _withAttributeContaining = new List<Dictionary<string, string>>();
        }

        public Xpath WithId(string id)
        {
            this._id = string.Format("@id='{0}'", id);
            return this;
        }
        public Xpath Descendant()
        {
            this._descendant = "//";
            this.CompleteXpath = string.Format("{0}{1}", this._descendant, this.CompleteXpath);
            return this;
        }

        public Xpath Child()
        {
            this._child = "/";
            this.CompleteXpath = string.Format("{0}{1}", this._child, this.CompleteXpath);
            return this;
        }

        public Xpath HavingParent(Xpath parentXpath)
        {
            this._havingParent = string.Format("{0}/", parentXpath.CompleteXpath);
            this.CompleteXpath = string.Format("{0}{1}", this._havingParent, this.CompleteXpath);
            return this;
        }

        public Xpath HavingAntecedent(Xpath antecedentXpath)
        {
            this._havingAntecedent = string.Format("{0}", antecedentXpath.CompleteXpath);
            this.CompleteXpath = string.Format("{0}{1}", this._havingAntecedent, this.CompleteXpath);
            return this;
        }

        public Xpath HavingAntecedent(Xpath antecedentXpath, Xpath descendentXpath)
        {
            var newXpath = new Xpath("")
            {
                CompleteXpath = string.Format("{0}{1}", antecedentXpath.CompleteXpath, descendentXpath.CompleteXpath)
            };

            return newXpath;
        }

        public Xpath WithExactClass(string className)
        {
            this._withExactClass = string.Format("@class='{0}'",  className);
            return this;
        }

        public Xpath WithClass(string className)
        {
            this._withClass = string.Format("contains(@class, '{0}')", className);
            return this;
        }

        public Xpath WithClasses(string classSequence)
        {
            this._withClasses = string.Format("@class='{0}'", classSequence); ;
            return this;
        }

        public Xpath WithExactText(string text)
        {
            this._withExactText = string.Format("text()='{0}'", text);
            return this;
        }

        public Xpath WithText(string text)
        {
            this._withText = string.Format("contains(text(), '{0}')", text);
            return this;
        }

        public Xpath WithTextStarting(string text)
        {
            this._withTextStarting = string.Format("starts-with(text(), '{0}')", text);
            return this;
        }

        public Xpath WithAttribute(string attriubte, string AttributeValue)
        {
            Dictionary<string, string> entry = new Dictionary<string, string> { { attriubte, AttributeValue } };
            this._withAttribute.Add(entry);
            return this;
        }

        public Xpath WithAttributeContaining(string attriubte, string AttributeValue)
        {
            Dictionary<string, string> entry = new Dictionary<string, string> { { attriubte, AttributeValue } };
            this._withAttributeContaining.Add(entry);
            return this;
        }

        public Xpath HavingChild(Xpath childXpath)
        {
            this._havingChild = string.Format(".{0}", childXpath.CompleteXpath);
            return this;
        }

        public Xpath HavingDescendant(Xpath descendantXpath)
        {
            this._havingDescendant = string.Format(".{0}", descendantXpath.CompleteXpath);
            return this;
        }

        public Xpath Parent()
        {
            this._parent += "/..";
            return this;
        }

        public Xpath Grandparent()
        {
            this._parent += "/../..";
            return this;
        }

        public Xpath Combined()
        {
            if ((_id != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(_id);
            }
            else if (_id != null)
            {
                this.PushToAPropertyArray(_id);
            }

            if ((ArrangeClasses() !=null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(ArrangeClasses());
            }
            else if (ArrangeClasses() != null)
            {
                this.PushToAPropertyArray(ArrangeClasses());
            }

            if ((ArrangeAttributes() != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(ArrangeAttributes());
            }
            else if (ArrangeAttributes() != null)
            {
                this.PushToAPropertyArray(ArrangeAttributes());
            }

            if ((ArrangeAttributesContaining() != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(ArrangeAttributesContaining());
            }
            else if (ArrangeAttributesContaining() != null)
            {
                this.PushToAPropertyArray(ArrangeAttributesContaining());
            }

            if ((ArrangeText() != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(ArrangeText());
            }
            else if (ArrangeText() != null)
            {
                this.PushToAPropertyArray(ArrangeText());
            }

            if ((_havingChild != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(this._havingChild);
            }
            else if (_havingChild != null)
            {
                this.PushToAPropertyArray(this._havingChild);
            }

            if ((_havingDescendant != null) && ArePropertiesDefined())
            {
                this.PushToAPropertyArray(" and ");
                this.PushToAPropertyArray(this._havingDescendant);
            }
            else if (_havingDescendant != null)
            {
                this.PushToAPropertyArray(this._havingDescendant);
            }

            if (_parent != null)
            {
                this.CompleteXpath += _parent;
            }

            return this;
        }

        private string ArrangeClasses()
        {
            if (this._withExactClass != null)
            {
                return this._withExactClass;
            }
            else if (this._withClasses != null)
            {
                return this._withClasses;
            }
            else if (this._withClass != null)
            {
                return this._withClass;
            }
            else
            {
                return null;
            }
        }

        private string ArrangeAttributes()
        {
            if (this._withAttribute.Count > 0)
            {
                List<string> attributes = new List<string>();
                this._withAttribute.ForEach((pair) => attributes.Add(string.Format("@{0}='{1}'", pair.Keys.ToList()[0], pair[pair.Keys.ToList()[0]])));
                return String.Join(" and ", attributes);
            } else
            {
                return null;
            }
        }

        private string ArrangeAttributesContaining()
        {
            if (this._withAttributeContaining != null && this._withAttributeContaining.Count > 0)
            {
                List<string> attributes = new List<string>();
                this._withAttributeContaining.ForEach((pair) => attributes.Add(string.Format("contains(@{0}, '{1}')", pair.Keys.ToList()[0], pair[pair.Keys.ToList()[0]])));
                return string.Join(" and ", attributes);
            }
            else
            {
                return null;
            }
        }

        private string ArrangeText()
        {
            if (this._withExactText != null)
            {
                return this._withExactText;
            }
            else if (this._withText != null)
            {
                return this._withText;
            }
            else if (this._withTextStarting !=null)
            {
                return this._withTextStarting;
            }
            else
            {
                return null;
            }
        }

        private bool ArePropertiesDefined()
        {
            int stringLength = this.CompleteXpath.Length;
            if ( '[' == this.CompleteXpath.ElementAt(stringLength - 2))
            {
                return false;
            } else
            {
                return true;
            }
        }

        private void PushToAPropertyArray(string addon)
        {
            StringBuilder builder = new StringBuilder(this.CompleteXpath);
            builder.Remove((builder.Length - 1), 1);
            StringBuilder helper = new StringBuilder(addon);
            builder.Append(helper.Append("]").ToString());
            this.CompleteXpath = builder.ToString();

        }

        public override string ToString()
        {
            return this.CompleteXpath;
        }

    }
}
