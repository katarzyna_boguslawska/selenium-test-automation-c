﻿using System;

namespace TrelloTestAutomation.Interfaces
{
    public interface ICloseable
    {
        void Close();
    }
}
