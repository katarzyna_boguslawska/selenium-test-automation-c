﻿using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Boards;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Members;
using TrelloTestAutomation.Objects.Pages.CommonComponents.TeamProfileComponents.Settings;

namespace TrelloTestAutomation.Interfaces
{
    public interface IProfilable
    {
        TeamMembers GoToMemebers();
        TeamSettings GoToSettings();
        TeamBoards GoToBoards();
        void AddMember(string who);
        void ChangeVisibility(Visibility toWhatVisibility);
        void DeleteTeam();
        void RemoveMember();
        bool IsNamed();
        bool IsPublicVisibilityDisplayed();
        bool IsPrivateVisibilityDisplayed();
        bool IsMemberAdded();
    }
}
