﻿namespace TrelloTestAutomation.Interfaces
{
    public interface ITeamable
    {
        void CompleteName(string name);
        void CompleteDescription(string description);
        IProfilable Create(string name);
        IProfilable Create(string name, string description);
    }
}
