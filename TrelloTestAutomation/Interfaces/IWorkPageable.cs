﻿using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.List;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu;
using TrelloTestAutomation.Objects.Pages.DeletedWorkPagePage;

namespace TrelloTestAutomation.Interfaces
{
    public interface IWorkPageable
    {
        void AddList(string listTitle);
        List FindListByTitle(string listTitle);
        void ArchiveListWithTitle(string listTitle);
        int CountLists();
        BoardMenu ShowBoardMenu();
        DeletedWorkPagePage DeleteBoard();
        void RenameBoard(string newName);
        bool IsTitled(string name);
        bool IsBackgroundColored(Color color);
        bool IsBackgroundImage();
    }
}
