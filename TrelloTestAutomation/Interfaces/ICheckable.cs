﻿using System;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.Interfaces
{
    internal interface ICheckable
    {
        bool IsPresent(Func<PageElement> toBeCalled);
        bool IsVisible(Func<PageElement> toBeCalled);
        bool IsClickable(Func<PageElement> toBeCalled);

    }
}
