﻿using System;
using TrelloTestAutomation.Interfaces;

namespace TrelloTestAutomation.Interfaces
{
    public interface ICreatorable<T>
    {
        void CompleteName(string name);
        void CompleteDescription(string description);
        T ClickCreate(string name);
        T CreateTeam(string name);
        T CreateTeam(string name, string description);
    }
}
