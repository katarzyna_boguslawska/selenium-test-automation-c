﻿using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.FixedChoices
{
    public sealed class Emoji
    {
        public Xpath EmojiLocator { get; }
        public Xpath EmojiImage { get; }

        private Emoji(Xpath emojiLocator, Xpath emojiImage)
        {
            this.EmojiLocator = emojiLocator;
            this.EmojiImage = emojiImage;
        }

        public static Emoji thumbsup = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "thumbsup").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "thumbsup").WithClass("emoji").Combined()
        );
        public static Emoji smile = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "smile").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "smile").WithClass("emoji").Combined()
        );
        public static Emoji warning = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "warning").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "warning").WithClass("emoji").Combined()
        );
        public static Emoji sunglasses = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "sunglasses").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "sunglasses").WithClass("emoji").Combined()
        );
        public static Emoji ballot = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "ballot_box_with_check").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "ballot_box_with_check").WithClass("emoji").Combined()
        );
        public static Emoji facepalm = new Emoji(
            new Xpath("a").Descendant().WithClasses("name js-select-emoji").WithAttribute("href", "#").WithAttribute("title", "facepalm").Combined(),
            new Xpath("img").Descendant().WithAttribute("title", "facepalm").WithClass("emoji").Combined()
        );
    }
}
