﻿using OpenQA.Selenium;
using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.FixedChoices
{
    public sealed class BoardMenuSubmenu
    {
        public Xpath SubmenuPositionLocator { get; }

        private BoardMenuSubmenu(Xpath submenuPositionLocator)
        {
            SubmenuPositionLocator = submenuPositionLocator;
        }

        private static Xpath _moreXpath = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-more").Combined();
        private static Xpath _backgroundXpath = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-change-background").Combined();
        private static Xpath _stickersXpath = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-stickers").Combined();
        private static Xpath _powerupsXpath = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-power-ups").Combined();
        private static Xpath _filterXpath = new Xpath("a").Descendant().WithClasses("board-menu-navigation-item-link js-open-card-filter").Combined();

        public static BoardMenuSubmenu More = new BoardMenuSubmenu(_moreXpath);
        public static BoardMenuSubmenu Background = new BoardMenuSubmenu(_backgroundXpath);
        public static BoardMenuSubmenu Stickers = new BoardMenuSubmenu(_stickersXpath);
        public static BoardMenuSubmenu Powerups = new BoardMenuSubmenu(_powerupsXpath);
        public static BoardMenuSubmenu Filter = new BoardMenuSubmenu(_filterXpath);
    }
}
