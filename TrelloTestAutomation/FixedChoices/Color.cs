﻿using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.FixedChoices
{
    public sealed class Color
    {
        public Xpath LabelLocator { get; }
        public Xpath EditLabelLocator { get; }
        public Xpath BackgroundLocator { get; }
        public Xpath CheckLabelLocator { get; }
        public string RGBcomponents { get; }

        private Color(Xpath labelLocator, Xpath editLabelLocator, Xpath backgroundLocator, 
            Xpath checkLabelLocator, string RGBcomponents)
        {
            LabelLocator = labelLocator;
            EditLabelLocator = editLabelLocator;
            BackgroundLocator = backgroundLocator;
            CheckLabelLocator = checkLabelLocator;
            this.RGBcomponents = RGBcomponents;
        }


        public static Color green = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-green  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-green palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(81, 152, 57);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-green mod-card-front").Combined(),
            "rgba(81, 152, 57, 1)"
        );

        public static Color yellow = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-yellow  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-yellow palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "no such color").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-yellow mod-card-front").Combined(),
	        "rgba(242, 214, 0, 1)"
        );

        public static Color orange = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-orange  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-orange palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(210, 144, 52);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-orange mod-card-front").Combined(),
            "rgba(210, 144, 52, 1)"
        );

        public static Color red = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-red  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-red palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(176, 50, 70);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-red mod-card-front").Combined(),
            "rgba(176, 50, 70, 1)"
        );

        public static Color purple = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-purple  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-purple palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(137, 96, 158);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-purple mod-card-front").Combined(),
            "rgba(137, 96, 158, 1)"
        );

        public static Color blue = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-blue  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-blue palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(0, 121, 191);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-blue mod-card-front").Combined(),
            "rgba(0, 121, 191, 1)"
        );

        public static Color turquoise = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-sky  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-sky palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(0, 174, 204);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-sky mod-card-front").Combined(),
            "rgba(0, 174, 204, 1)"
        );

        public static Color pistachio = new Color(
            new Xpath("span").Descendant().WithClasses("card-label mod-selectable card-label-lime  js-select-label").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-lime palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(75, 191, 107);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-lime mod-card-front").Combined(),
            "rgba(75, 191, 107, 1)"
        );

        public static Color pink = new Color(
            new Xpath("span").Descendant().WithClasses("no such color").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-pink palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(205, 90, 145);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-pink mod-card-front").Combined(),
            "rgba(205, 90, 145, 1)"
        );

        public static Color gray = new Color(
            new Xpath("span").Descendant().WithClasses("no such color").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label mod-edit-label mod-clickable card-label-black palette-color js-palette-color").Combined(),
            new Xpath("div").Descendant().WithClass("image").WithAttribute("style", "background-color: rgb(131, 140, 145);").Combined(),
            new Xpath("span").Descendant().WithClasses("card-label card-label-black mod-card-front").Combined(),
            "rgba(131, 140, 145, 1)"
        );
    }
}
