﻿using TrelloTestAutomation.Objects.Base;

namespace TrelloTestAutomation.FixedChoices
{
    public sealed class Visibility
    {
        public Xpath CurrentVisibilityLocator { get;  }
        public Xpath VisibilityButtonLocator { get; }

        private Visibility(Xpath currentVisibilityLocator, Xpath visibilityButtonLocator)
        {
            this.CurrentVisibilityLocator = currentVisibilityLocator;
            this.VisibilityButtonLocator = visibilityButtonLocator;
        }

        private static Xpath _privCurrentVisibilityXpath = new Xpath("span").Descendant().WithExactText("Private").Combined();
        private static Xpath _privVisibilityButtonXpath = new Xpath("a").Descendant().WithClasses("highlight-icon js-select-members").Combined();
        private static Xpath _pubCurrentVisibilityXpath = new Xpath("span").Descendant().WithExactText("Public").Combined();
        private static Xpath _pubVisibilityButtonXpath = new Xpath("a").Descendant().WithClasses("highlight-icon js-select-public").Combined();


        public static Visibility privateVisibility = new Visibility(_privCurrentVisibilityXpath, _privVisibilityButtonXpath);
        public static Visibility publicVisibility = new Visibility(_pubCurrentVisibilityXpath, _pubVisibilityButtonXpath);
    }
}
