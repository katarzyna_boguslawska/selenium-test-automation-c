﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Objects.Pages.PersonalTeamProfilePage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class TeamManipulationTests
    {
        private static ChromeDriver _driver;
        private static TestUser _user;
        private static LoggedInPage _loggedIn;
        private static PersonalTeamProfile _personalTeam;

        [SetUp]
        public void LogIn()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            LoginPage loginPage = new LoginPage(_driver);
            _user = new TestUser();
            _loggedIn = loginPage.LogIn(_user.Login, _user.Password);
        }
        [Test]
        public void CreateTeam()
        {
            _personalTeam = _loggedIn.CreatePersonalTeam("testTeam");
            Assert.IsTrue(_personalTeam.IsNamed());
        }

        [Test]
        public void ChangeVisibilityToPublic()
        {
            _personalTeam = _loggedIn.CreatePersonalTeam("testTeam");
            _personalTeam.ChangeVisibility(Visibility.publicVisibility);
            Assert.IsTrue(_personalTeam.IsPublicVisibilityDisplayed());
            _personalTeam.ChangeVisibility(Visibility.privateVisibility);
        }

        [Test]
        public void AddMemeberToTeam()
        {
            _personalTeam = _loggedIn.CreatePersonalTeam("testTeam");
            _personalTeam.AddMember(testSettings.Default.NEW_MEMBER_EMAIL);
            Assert.IsTrue(_personalTeam.IsMemberAdded());
            // _personalTeam.RemoveMember();
        }

        [TearDown]
        public void DeleteBoardAndLogOut()
        {
            _personalTeam.DeleteTeam();
            _loggedIn.IsCreateBoardPossible();
            _loggedIn.LogOut();
            _driver.Quit();
        }
    }
}
