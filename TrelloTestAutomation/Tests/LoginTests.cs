﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TrelloTestAutomation.Objects.Pages.FailedLoginPage;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Objects.Pages.PasswordResetPage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class LoginTests
    {
        private static IWebDriver _driver;
        private static LoginPage _loginPage;
        private static TestUser _user;

        [SetUp]
        public void OpenBrowser()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            _loginPage = new LoginPage(_driver);
            _user = new TestUser();
        }

        [Test]
        public void LogInSuccessfully()
        {
            
            LoggedInPage loggedPage = _loginPage.LogIn(_user.Login, _user.Password);
            Assert.IsTrue(loggedPage.IsCreateBoardPossible());
            loggedPage.LogOut();
        }

        [Test]
        public void FailLoginDueToPassword()
        {
            _loginPage.CompleteLogin(_user.Login);
            _loginPage.CompletePassword(_user.InvalidPassword);
            FailedLoginPage failurePage = _loginPage.FailLogin();
            Assert.IsTrue(failurePage.IsErrorCommunicated());
        }

        [Test]
        public void FailLoginDueToUsername()
        {
            _loginPage.CompleteLogin(_user.InvalidLogin);
            _loginPage.CompletePassword(_user.Password);
            FailedLoginPage failurePage = _loginPage.FailLogin();
            Assert.IsTrue(failurePage.IsErrorCommunicated());
        }

        [Test]
        public void ResetPassword()
        {
            PasswordResetPage passwordResetPage = _loginPage.RequestPasswordReset(_user.Login);
            passwordResetPage.CompleteDataForReset(_user.Login);
            Assert.IsTrue(passwordResetPage.IsEmailSent());
        }

        [TearDown]
        public void CloseBrowser()
        {
            _driver.Quit();
        }
    }
}
