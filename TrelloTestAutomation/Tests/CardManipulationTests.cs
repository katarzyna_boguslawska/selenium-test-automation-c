﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using System;
using System.Diagnostics;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.GroupWorkPage;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class CardManipulationTests
    {
        private static ChromeDriver _driver;
        private static TestUser _user;
        private static LoggedInPage _loggedIn;
        private static GroupWorkPage _workpage;
        private static Objects.Pages.CommonComponents.WorkpageComponents.List.List list1;
        private static Objects.Pages.CommonComponents.WorkpageComponents.List.List list2;
        private static Objects.Pages.CommonComponents.WorkpageComponents.List.List list3;
        private static Objects.Pages.CommonComponents.WorkpageComponents.List.List list4;

        [OneTimeSetUp]
        public void LogInAndPrepareBoard()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            LoginPage loginPage = new LoginPage(_driver);
            _user = new TestUser();
            _loggedIn = loginPage.LogIn(_user.Login, _user.Password);
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "Test group board");
            _workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];

            for (int i = 0; i < 4; i++)
            {
                string listTitle = "List " + (i + 1).ToString();
                _workpage.AddList(listTitle);
            }
            list1 = _workpage.FindListByTitle("List 1");
            list2 = _workpage.FindListByTitle("List 2");
            list3 = _workpage.FindListByTitle("List 3");
            list4 = _workpage.FindListByTitle("List 4");

            for (int i = 0; i < 6; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 1";
                list1.AddCard(cardTitle);
            }
            for (int i = 0; i < 3; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 2";
                list2.AddCard(cardTitle);
            }
            for (int i = 0; i < 4; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 3";
                list3.AddCard(cardTitle);
            }
            for (int i = 0; i < 3; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 4";
                list4.AddCard(cardTitle);
            }
        }

        [Test]
        public void AddDueDate()
        {
            list1.AddDueDateOnCardWithText("Card 1 on List 1", "11/28/2017");
            Assert.IsTrue(list1.WasDueDateAddedToCardWithText("Card 1 on List 1"));
        }

        [Test]
        public void LabelCard()
        {
            list3.LabelCardWithText("Card 1 on List 3", Color.orange);
            Assert.IsTrue(list3.WasCardWithTextLabelled("Card 1 on List 3", Color.orange));
        }

        [Test]
        public void AssignToSomebody()
        {
            list1.AssignMemberToCardWithText("Card 4 on List 1", "Python Wsb");
            Assert.IsTrue(list1.WasCardWithTextAssignedTo("Card 4 on List 1", "Python Wsb"));
        }

        [Test]
        public void DragCard()
        {
            list1.DragCardWithTextInPlaceOfCardWithText("Card 6 on List 1", "Card 3 on List 2", list2);
            Assert.IsTrue(list1.WasCardWithTextDroppedInPlaceOfCardWithText("Card 6 on List 1", list2));
        }

        [Test]
        public void CommentCard()
        {
            list3.CommentCardWithText("Card 3 on List 3", "Test comment #1");
            Assert.IsTrue(list3.WasCardWithTextCommented("Card 3 on List 3", "Test comment #1"));
        }

        [Test]
        public void CommentWithEmoji()
        {
            list4.CommentCardWithTextAndAddEmoji("Card 1 on List 4", "FFS...", Emoji.facepalm);
            Assert.IsTrue(list4.WasCardWithTextCommented("Card 1 on List 4", "FFS...", Emoji.facepalm));
        }

        [Test]
        public void AddAttachmentFromLink()
        {
            list3.AddAttachmentFromLinkToCardWithText("Card 4 on List 3", _user.LinkToAttachment);
            Assert.IsTrue(list3.WasAttachmentAddedToCardWithText("Card 4 on List 3"));
        }

        [Test]
        public void RemoveComment()
        {
            list4.CommentCardWithText("Card 2 on List 4", "This will be gone");
            list4.DeleteCommentFromCardWithText("Card 2 on List 4", "This will be gone");
            Assert.IsTrue(list4.WasCommentRemovedFromCardWithText("Card 2 on List 4", "This will be gone"));
        }

        [Test]
        public void ArchiveCard()
        {
            list1.ArchiveCardWithText("Card 2 on List 1");
            Assert.IsTrue(list1.WasCardWithTextArchived("Card 2 on List 1"));
        }

        [Test]
        public void AddChecklist()
        {
            list1.AddChecklistToCardWithText("Card 5 on List 1", "Urgent");
            Assert.IsTrue(list1.WasChecklistAddedToCardWithText("Card 5 on List 1", "Urgent"));
        }

        [Test]
        public void AddPopulatedChecklist()
        {
            list1.AddChecklistToCardWithText("Card 3 on List 1", "Urgent", "Task #");
            Assert.IsTrue(list1.WasChecklistPopulatedOnCardWithText("Card 3 on List 1", "Urgent"));
        }

        [Test]
        public void MakeProgressWithChecklistItems()
        {
            list3.AddChecklistToCardWithText("Card 2 on List 3", "Urgent", "Task #");
            string[] tasks = { "Task #2", "Task #3" };
            list3.CompleteListedTasksOnCardWithText("Card 2 on List 3", "Urgent", tasks);
            Assert.IsTrue(list3.WereTasksCompleted("Card 2 on List 3", "Urgent", 2));
        }

        [OneTimeTearDown]
        public static void CleanUpAndExit()
        {
            _workpage.DeleteBoard();
            _workpage.LogOut();
            _driver.Quit();
        }
    }
}
