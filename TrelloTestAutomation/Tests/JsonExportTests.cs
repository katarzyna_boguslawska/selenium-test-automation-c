﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using TrelloTestAutomation.Objects.Pages.GroupWorkPage;
using TrelloTestAutomation.Objects.Pages.JsonExportedPage;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class JsonExportTests
    {
        private static ChromeDriver _driver;
        private static TestUser _user;
        private static LoggedInPage _loggedIn;
        private static GroupWorkPage _workpage;
        private static Objects.Pages.CommonComponents.WorkpageComponents.List.List list1;

        [SetUp]
        public void OpenBrowserAndLogIn()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            LoginPage loginPage = new LoginPage(_driver);
            _user = new TestUser();
            _loggedIn = loginPage.LogIn(_user.Login, _user.Password);
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "Test group board");
            _workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];
            _workpage.AddList("List 1");
            list1 = _workpage.FindListByTitle("List 1");
            for (int i = 0; i < 6; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 1";
                list1.AddCard(cardTitle);
            }
        }

        [Test]
        public void ExportToJson()
        {
            JsonExportedPage jsonPage = list1.ExportToJsonCardWithText("Card 3 on List 1");
            Assert.IsTrue(jsonPage.IsJson());
            _workpage = jsonPage.UnexportFromJson(_workpage);
        }

        [TearDown]
        public void CleanUpAndExit()
        {
            _workpage.DeleteBoard();
            _loggedIn.LogOut();
            _driver.Quit();
        }
    }
}
