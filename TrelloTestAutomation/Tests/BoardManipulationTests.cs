﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using TrelloTestAutomation.Objects.Pages.DeletedWorkPagePage;
using TrelloTestAutomation.Objects.Pages.GroupWorkPage;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Objects.Pages.SearchResultsPage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class BoardManipulationTests
    {
        private static ChromeDriver _driver;
        private static LoggedInPage _loggedIn;
        private static TestUser _user;

        [SetUp]
        public void OpenBrowserAndLogIn()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            _user = new TestUser();
            LoginPage loginPage = new LoginPage(_driver);
            _loggedIn = loginPage.LogIn(_user.Login, _user.Password);
        }

        [Test]
        public void CreateWorkPage()
        {
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "New Board");
            GroupWorkPage workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];
            Assert.IsTrue(workpage.IsTitled("New Board"));
            workpage.DeleteBoard();
        }

        [Test]
        public void RenameWorkPage()
        {
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "New Board");
            GroupWorkPage workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];
            workpage.RenameBoard("Newly Named Board");
            Assert.IsTrue(workpage.IsTitled("Newly Named Board"));
            workpage.DeleteBoard();
        }
        
        [Test]
        public void SearchForWorkPage()
        {
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "New Board");
            GroupWorkPage workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];
            SearchResultsPage results = workpage.SearchForBoard("New Board");
            Assert.IsTrue(results.AreResultsPresent());
            //results.GoToHomeDashboard().GoToWorkPage(testSettings.Default.USER_GROUP, "New Board").DeleteBoard();
        }

        [Test]
        public void DeleteWorkPage()
        {
            _loggedIn.AddNewGroupBoard(_user.DefaultWorkspace, "New Board");
            GroupWorkPage workpage = (GroupWorkPage)_loggedIn.GroupBoards[0];
            DeletedWorkPagePage deletedBoardPage = workpage.DeleteBoard();
            Assert.IsTrue(deletedBoardPage.IsDeleted());
        }

        [TearDown]
        public void CleanUpAndExit()
        {
            _loggedIn.LogOut();
            _driver.Quit();
        }
    }
}
