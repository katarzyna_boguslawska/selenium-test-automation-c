﻿using NUnit.Framework;
using OpenQA.Selenium.Chrome;
using TrelloTestAutomation.FixedChoices;
using TrelloTestAutomation.Objects.Pages.CommonComponents.WorkpageComponents.Menu;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Objects.Pages.PersonalWorkPage;
using TrelloTestAutomation.Users;

namespace TrelloTestAutomation.Tests
{
    public class WorkPageBackgroundManipulationTests
    {
        private static ChromeDriver _driver;
        private static TestUser _user;
        private static LoggedInPage _loggedIn;
        private static PersonalWorkPage _workpage;

        [OneTimeSetUp]
        public void LogInAndPrepareWorkPage()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            LoginPage loginPage = new LoginPage(_driver);
            _user = new TestUser();
            _loggedIn = loginPage.LogIn(_user.Login, _user.Password);
            _loggedIn.AddNewPersonalBoard("Test group board");
            _workpage = (PersonalWorkPage)_loggedIn.PersonalBoards[0];
        }

        [Test]
        public void ChangeBackgroundToColor()
        {
            BoardMenu menu = _workpage.ShowBoardMenu();
            menu.ChangeBackgroundToColor(Color.pistachio);
            Assert.IsTrue(_workpage.IsBackgroundColored(Color.pistachio));
        }

        [Test]
        public void ChangeBackgroundToPhoto()
        {
            BoardMenu menu = _workpage.ShowBoardMenu();
            menu.ChangeBackgroundToRandomPhoto();
            Assert.IsTrue(_workpage.IsBackgroundImage());
        }

        [TearDown]
        public void ReturnToBasicMenu()
        {
            BoardMenu menu = _workpage.ShowBoardMenu();
            menu.ReturnToMainMenu();
        }
        [OneTimeTearDown]
        public void LogOut()
        {
            _workpage.DeleteBoard();
            _loggedIn.LogOut();
            _driver.Quit();
        }
    }
}
