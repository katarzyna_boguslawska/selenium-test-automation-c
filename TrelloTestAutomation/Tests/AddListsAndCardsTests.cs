﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using NUnit.Framework;
using TrelloTestAutomation.Users;
using TrelloTestAutomation.Objects.Pages.LoginPage;
using TrelloTestAutomation.Objects.Pages.LoggedInPage;
using TrelloTestAutomation.Objects.Pages.GroupWorkPage;

namespace TrelloTestAutomation.Tests
{
    public class AddListsAndCardsTests
    {
        private static IWebDriver _driver;
        private static LoginPage _loginPage;
        private static LoggedInPage _loggedInPage;
        private static GroupWorkPage _groupWorkPage;
        private static TestUser _user;

        [SetUp]
        public void OpenBrowserAndLogIn()
        {
            ChromeOptions options = new ChromeOptions();
            options.AddArgument("--start-maximized");
            _driver = new ChromeDriver(options);
            _loginPage = new LoginPage(_driver);
            _user = new TestUser();
            _loggedInPage = _loginPage.LogIn(_user.Login, _user.Password);
            _loggedInPage.AddNewGroupBoard(_user.DefaultWorkspace, "Testing 1");
            _groupWorkPage = _loggedInPage.GroupBoards[0];
        }
        [Test]
        public void AddLists()
        {
            _groupWorkPage.AddList("List 1");
            _groupWorkPage.AddList("List 2");
            Assert.That(_groupWorkPage.CountLists().Equals(2));
        }

        [Test]
        public void AddCards()
        {
            _groupWorkPage.AddList("List 1");
            Objects.Pages.CommonComponents.WorkpageComponents.List.List list1 = _groupWorkPage.FindListByTitle("List 1");
            for (int i = 0; i < 4; i++)
            {
                string cardTitle = "Card " + (i + 1).ToString() + " on List 1";
                list1.AddCard(cardTitle);
            }
        }

        [TearDown]
        public void LogOutAndClose()
        {
            _groupWorkPage.DeleteBoard();
            _loggedInPage.LogOut();
            _driver.Quit();
        }
    }
}
