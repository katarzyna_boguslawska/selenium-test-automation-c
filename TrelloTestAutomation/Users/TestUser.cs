﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrelloTestAutomation.Users
{
    class TestUser
    {
        public string Login { get; }
        public string Password { get; }
        public string DefaultWorkspace { get; }
        public string InvalidLogin { get; set; }
        public string InvalidPassword { get; set; }
        public string NewGruopToCreate { get; set; }
        public string LinkToAttachment { get; set; }

        public TestUser(string login = null, string password = null, string defaultWorkspace = null,
            string invalidLogin = null, string invalidPassword = null, string newGroup = null, string link = null)
        {
            Login = login ?? testSettings.Default.VALID_USER;
            Password = password ?? testSettings.Default.VALID_PASSWORD;
            DefaultWorkspace = defaultWorkspace ?? testSettings.Default.USER_GROUP;
            InvalidLogin = invalidLogin ?? testSettings.Default.INVALID_USER;
            InvalidPassword = invalidLogin ?? testSettings.Default.INVALID_PASSWORD;
            NewGruopToCreate = newGroup ?? testSettings.Default.NEW_TEAM;
            LinkToAttachment = link ?? testSettings.Default.ATTACHMENT_LINK;

        }
    }
}
